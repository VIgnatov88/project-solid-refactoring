﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TeamWork_Project.Contracts;
using WorkItemManager.Core;
using WorkItemManager.Entities;

namespace Project.Tests.MembersTests
{
    [TestClass]
    public class Members_Should
    {
        [TestMethod]
        public void AddsPersonWhenValidArguments()
        {
            //Arrange
            var name = "Gosho";
            var fakePerson = new Mock<IPerson>();
            fakePerson.SetupGet(p => p.Name).Returns(name);
            Members members = new Members();

            // Act, Assert
            members.AddPerson(fakePerson.Object);
            Assert.AreNotEqual(null, members.GetPerson(name));
        }

        [TestMethod]
        public void GetPersonThrowExeptionWhenPersonIsNotFound()
        {
            //Arrange
            var name = "Gosho";
            var fakePerson = new Mock<IPerson>();
            fakePerson.SetupGet(p => p.Name).Returns(name);
            Members members = new Members();
            members.AddPerson(fakePerson.Object);

            // Act, Assert
            var ex = Assert.ThrowsException<ArgumentException>(
                () => members.GetPerson("Pesho"));

            Assert.AreEqual("No such person!", ex.Message);
        }

        [TestMethod]
        public void ShowAllPeopleThrowExeptionWhenNoPeople()
        {
            //Arrange
            Members members = new Members();
            var ex = Assert.ThrowsException<ArgumentException>(
                () => members.ShowAllPeople());

            // Act, Assert
            Assert.AreEqual("There are no people!", ex.Message);
        }

        [TestMethod]
        public void ShowAllPeopleWhenValidPeople()
        {
            //Arrange
            Members members = new Members();

            var fakePerson1 = new Mock<IPerson>();
            fakePerson1.SetupGet(p => p.Name).Returns("Gosho");
            var fakePerson2 = new Mock<IPerson>();
            fakePerson2.SetupGet(p => p.Name).Returns("Pesho");

            members.AddPerson(fakePerson1.Object);
            members.AddPerson(fakePerson2.Object);

            // Act, Assert
            Assert.AreEqual(" - Gosho\r\n - Pesho", members.ShowAllPeople());
        }

        [TestMethod]
        public void ContainsMemberWhenValidInput()
        {
            //Arrange
            Members members = new Members();

            var fakePerson1 = new Mock<IPerson>();
            fakePerson1.SetupGet(p => p.Name).Returns("Gosho");
            var fakePerson2 = new Mock<IPerson>();
            fakePerson2.SetupGet(p => p.Name).Returns("Pesho");

            members.AddPerson(fakePerson1.Object);
            members.AddPerson(fakePerson2.Object);

            // Act, Assert
            Assert.AreEqual(true, members.ContainsMember("Gosho"));
        }

        [TestMethod]
        public void ContainsMemberWhenInvalidInput()
        {
            //Arrange
            Members members = new Members();

            var fakePerson1 = new Mock<IPerson>();
            fakePerson1.SetupGet(p => p.Name).Returns("Gosho");
            var fakePerson2 = new Mock<IPerson>();
            fakePerson2.SetupGet(p => p.Name).Returns("Pesho");

            members.AddPerson(fakePerson1.Object);
            members.AddPerson(fakePerson2.Object);

            // Act, Assert
            Assert.AreNotEqual(true, members.ContainsMember("Misho"));
        }
    }
}

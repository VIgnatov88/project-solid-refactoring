﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using TeamWork_Project.Contracts;
using WorkItemManager.Entities;
using WorkItemManager.WorkIteams;

namespace Project.Tests.WorkItemsTests
{
    [TestClass]

    public class Team_Should
    {
        [TestMethod]

        [DataRow(null)]
        [DataRow("")]

        public void ThrowsExceptionWhen_NameIsNullOrEmpty(string invalidParameters)
        {
            var ex = Assert.ThrowsException<ArgumentException>(
               () => new Team(invalidParameters));

            Assert.AreEqual("Name cannot be null or empty", ex.Message);
        }

        [TestMethod]

        [DataRow("aaa")]
        [DataRow("aaaaaaaaaaaaaaaaaa")]

        public void ThrowsExceptionWhen_IncorrectNameParameter_IsPassed(string invalidParameters)
        {
            var ex = Assert.ThrowsException<ArgumentException>(
               () => new Team(invalidParameters));

            Assert.AreEqual("Name must be between 5 and 15 characters inclusive", ex.Message);
        }

        [TestMethod]

        public void Constructor_CorrectlyInitializes_Collections()
        {
            var team = new Team("Champions");

            Assert.IsNotNull(team.ActivityHistory);
            Assert.IsNotNull(team.Boards);
            Assert.IsNotNull(team.Members);
        }

        [TestMethod]

        public void AddPerson_CorrectlyAdds_NewPersonToCollection()
        {
            var team = new Team("Champions");
            var fakePerson = new Mock<IPerson>();
            fakePerson.SetupGet(p => p.Name).Returns("Pesho");

            team.AddMember(fakePerson.Object);

            Assert.AreNotEqual(0, team.Members);
        }

        [TestMethod]

        public void SetBoard_ThrowsException_IfPassedBoard_AlreadyExists()
        {
            var team = new Team("Champions");
            var fakeBoard = new Mock<IBoard>();
            fakeBoard.SetupGet(b => b.Name).Returns("fakeName");
            team.SetBoard(fakeBoard.Object);

            var ex = Assert.ThrowsException<ArgumentException>(
                () => team.SetBoard(fakeBoard.Object));

            Assert.AreEqual("Such board already exists!", ex.Message);
        }

        [TestMethod]

        public void GetAllBoards_Prints_AllExistingBoards_InTeam()
        {
            var team = new Team("Champions");

            var fakeBoard = new Mock<IBoard>();
            fakeBoard.SetupGet(b => b.Name).Returns("fakeName");

            var fakeBoard2 = new Mock<IBoard>();
            fakeBoard2.SetupGet(b => b.Name).Returns("fakeName2");

            team.SetBoard(fakeBoard.Object);
            team.SetBoard(fakeBoard2.Object);


            Assert.AreEqual(" - fakeName\r\n - fakeName2", team.GetAllBoards());
        }

        //[TestMethod]  // NOT Working!

        //public void AddToBoardHistory_CorrectlyAddsComment()
        //{
        //    var team = new Team("Champions");

        //    var fakeBoard = new Mock<IBoard>();
        //    fakeBoard.SetupGet(b => b.Name).Returns("fakeName");
        //    fakeBoard.SetupGet(b => b.ActivityHistory).Returns(new List<History>());
        //    fakeBoard.Setup(b => b.AddToHistory(It.IsAny<string>()));

        //    team.SetBoard(fakeBoard.Object);
        //    team.AddToBoardHistory(fakeBoard.Object, "someStringToAdd");

        //    //var fakeHistory = new Mock<History>();

        //    Assert.AreNotEqual(0, fakeBoard.Object.ActivityHistory.Capacity);
        //}
    }
}

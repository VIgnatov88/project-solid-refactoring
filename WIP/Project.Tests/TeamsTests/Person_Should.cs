﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using TeamWork_Project.Contracts;
using WorkItemManager.Entities;

namespace Project.Tests.WorkItemsTests
{
    [TestClass]

    public class Person_Should
    {
        [TestMethod]

        [DataRow(null)]
        [DataRow("")]

        public void ThrowsExceptionWhen_NameIsNullOrEmpty(string invalidParameters)
        {
            var ex = Assert.ThrowsException<ArgumentException>(
               () => new Person(invalidParameters));

            Assert.AreEqual("Name cannot be null or empty", ex.Message);
        }

        [TestMethod]

        [DataRow("aaa")]
        [DataRow("aaaaaaaaaaaaaaaaaa")]

        public void ThrowsExceptionWhen_IncorrectNameParameter_IsPassed(string invalidParameters)
        {
            var ex = Assert.ThrowsException<ArgumentException>(
               () => new Person(invalidParameters));

            Assert.AreEqual("Name must be between 5 and 15 characters inclusive", ex.Message);
        }

        [TestMethod]

        public void Constructor_CorrectlyInitializes_Collections()
        {
            var person = new Person("Gosho");

            Assert.IsNotNull(person.ActivityHistory);
            Assert.IsNotNull(person.WorkItems);
        }

        [TestMethod]

        public void RemoveWorkItem_CorrectlyRemovesItem_FromCollections()
        {
            var person = new Person("Gosho");
            var fakeWorkItem1 = new Mock<IWorkItem>();
            var fakeWorkItem2 = new Mock<IWorkItem>();
            fakeWorkItem1.SetupGet(w => w.Title).Returns("WorkItemToRemove");
            fakeWorkItem2.SetupGet(w => w.Title).Returns("someWorkItem");
            
            person.WorkItems.Add(fakeWorkItem1.Object);
            person.WorkItems.Add(fakeWorkItem2.Object);

            person.RemoveWorkItem("WorkItemToRemove");

            Assert.AreEqual(1, person.WorkItems.Count);
        }

        [TestMethod]

        public void ReturnTrue_IfContainsWorkitem()
        {
            var person = new Person("Gosho");
            var workItem1 = new Mock<IWorkItem>();
            var workItem2 = new Mock<IWorkItem>();

            workItem1.SetupGet(w => w.Title).Returns("WorkItemToRemove");
            workItem2.SetupGet(w => w.Title).Returns("someWorkItem");

            person.WorkItems.Add(workItem1.Object);
            person.WorkItems.Add(workItem2.Object);

            var isContaining = person.CheckIfWorkItemExists("WorkItemToRemove");

            Assert.AreEqual(true, isContaining);
        }

        [TestMethod]

        public void ReturnFalse_IfDoesntContainWorkitem()
        {
            var person = new Person("Gosho");
            var workItem1 = new Mock<IWorkItem>();
            var workItem2 = new Mock<IWorkItem>();

            workItem1.SetupGet(w => w.Title).Returns("WorkItemToRemove");
            workItem2.SetupGet(w => w.Title).Returns("someWorkItem");

            person.WorkItems.Add(workItem1.Object);
            person.WorkItems.Add(workItem2.Object);

            var isContaining = person.CheckIfWorkItemExists("UnexistingItem");

            Assert.AreEqual(false, isContaining);
        }

        [TestMethod]

        public void GetWorkItem_ThrowsException_WhenWorkItem_IsNull()
        {
            var person = new Person("Gosho");
            var workItem1 = new Mock<IWorkItem>();
            var workItem2 = new Mock<IWorkItem>();

            workItem1.SetupGet(w => w.Title).Returns("WorkItemToRemove");
            workItem2.SetupGet(w => w.Title).Returns("someWorkItem");

            person.WorkItems.Add(workItem1.Object);
            person.WorkItems.Add(workItem2.Object);
            //var newItem = new Mock<IWorkItem>();
            var ex = Assert.ThrowsException<ArgumentException>(
                () => person.GetWorkItem("UnexistingItem"));

            Assert.AreEqual("No such Item!", ex.Message);
        }
    }
}

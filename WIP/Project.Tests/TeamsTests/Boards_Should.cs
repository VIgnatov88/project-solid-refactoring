﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using TeamWork_Project.Contracts;
using WorkItemManager.Entities;
using WorkItemManager.WorkIteams;

namespace Project.Tests.WorkItemsTests
{
    [TestClass]

    public class Boards_Should
    {
        [DataRow(null)]
        [DataRow("")]

        public void ThrowsExceptionWhen_NameIsNullOrEmpty(string invalidParameters)
        {
            var ex = Assert.ThrowsException<ArgumentException>(
               () => new Boards(invalidParameters));

            Assert.AreEqual("Name cannot be null or empty", ex.Message);
        }

        [TestMethod]

        [DataRow("aaa")]
        [DataRow("aaaaaaaaaaaaaaaaaa")]

        public void ThrowsExceptionWhen_IncorrectNameParameter_IsPassed(string invalidParameters)
        {
            var ex = Assert.ThrowsException<ArgumentException>(
               () => new Boards(invalidParameters));

            Assert.AreEqual("Board name must be between 5 and 10 characters inclusive", ex.Message);
        }

        [TestMethod]

        public void Constructor_CorrectlyInitializes_Collections()
        {
            var board = new Boards("someName");

            Assert.IsNotNull(board.ActivityHistory);
            Assert.IsNotNull(board.PossibleAuthors);
            Assert.IsNotNull(board.WorkItems);
        }

        [TestMethod]

        public void WorkItemExistsInBoard_ReturnsTrue_WhenItemExists()
        {
            var board = new Boards("someName");
            var WorkItem = new Bug();
            WorkItem.Title = "someTitleOfBug";

            board.WorkItems.Add(WorkItem);

            Assert.AreEqual(true, board.WorkItemExistsInBoard(WorkItem.Title, "Bug"));
        }

        [TestMethod]

        public void ThrowExceptionWhen_FindWorkItemByTitle_CannotFindItem()
        {
            var board = new Boards("someName");
            var WorkItem = new Bug();
            WorkItem.Title = "someTitleOfBug";

            var ex = Assert.ThrowsException<ArgumentException>(
                () => board.FindWorkItemByTitle(WorkItem.Title, "Story"));

            Assert.AreEqual("Workitem with such title does not exist", ex.Message);
        }

        [TestMethod]

        public void FindWorkItemByTitle_ReturnWorkItem()
        {
            var board = new Boards("someName");
            var WorkItem = new Bug();
            WorkItem.Title = "someTitleOfBug";
            board.AddWorkitemToBoard(WorkItem);

            var foundWorkItem = board.FindWorkItemByTitle(WorkItem.Title, "bug");

            Assert.AreEqual(foundWorkItem, WorkItem);
        }


        [TestMethod]

        public void AddToHistory_WorksCorrectly()
        {
            var board = new Boards("someName");
            board.AddToHistory("someHistoryString");

            Assert.AreEqual(1, board.ActivityHistory.Capacity);
        }
    }

}

﻿
using WorkItemManager.Entities;

namespace Project.Tests.CoreTests
{
    using WorkItemManager.Core;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;

    [TestClass]
    public class Teams_Should
    {
        [TestMethod]
        public void NotThrowsExeptionWhenValidArguments()
        {
            //Arrange
            string name = "teamName";
            Teams teams = new Teams();

            // Act, Assert
            teams.AddTeam(name);
            Assert.IsNotNull(teams.GetTeam(name));
        }

        [TestMethod]
        public void NotThrowsExeptionWhenShowAllTeams()
        {
            //Arrange
            string firstTeam = "firstTeam";
            string secondTeam = "secondTeam";
            Teams teams = new Teams();

            teams.AddTeam(firstTeam);
            teams.AddTeam(secondTeam);

            // Act, Assert
            Assert.AreNotEqual("There aren't any teams created", teams.ShowAllTeams());
        }

        [TestMethod]
        public void ThrowsExeptionWhenShowAllTeams()
        {
            // Arrange, Act, Assert
            Assert.ThrowsException<ArgumentException>(() =>
                new Teams().ShowAllTeams());
        }

        [TestMethod]
        public void NotThrowsNullExeptionWhenShowAllTeamMembers()
        {
            //Arrange
            string firstTeam = "firstTeam";
            Teams teams = new Teams();

            teams.AddTeam(firstTeam);

            teams.GetTeam(firstTeam).AddMember(new Person("Pesho"));
            teams.GetTeam(firstTeam).AddMember(new Person("Gosho"));

            Assert.IsNotNull(teams.ShowAllTeamMembers(firstTeam));
        }

        [TestMethod]
        public void NotThrowsArgumentExeptionWhenShowAllTeamMembers()
        {
            //Arrange
            string firstTeam = "firstTeam";
            Teams teams = new Teams();
            teams.AddTeam(firstTeam);

            // Act, Assert
            teams.GetTeam(firstTeam).AddMember(new Person("Pesho"));
            teams.GetTeam(firstTeam).AddMember(new Person("Gosho"));
            string output = teams.ShowAllTeamMembers(firstTeam);

            Assert.AreNotEqual("NO Team with name {teamName}!", output);
        }

        [TestMethod]
        public void ThrowsExeptionWhenShowAllTeamMembers()
        {
            // Arrange, Act, Assert
            Assert.ThrowsException<ArgumentException>(() =>
                new Teams().ShowAllTeamMembers("TeamName"));
        }

        [TestMethod]
        public void NotThrowsNullExeptionWhenGetTeam()
        {
            // Arrange, Act, Assert
            Teams teams = new Teams();
            teams.AddTeam("team");

            Assert.IsNotNull(teams.GetTeam("team"));
        }

        [TestMethod]
        public void NotThrowsExeptionWhenGetTeam()
        {
            // Arrange, Act, Assert
            Teams teams = new Teams();
            teams.AddTeam("team");

            Assert.AreNotEqual(
                new ArgumentException("No such team!"), teams.GetTeam("team"));
        }

        [TestMethod]
        public void ThrowsExeptionWhenGetTeam()
        {
            // Arrange, Act, Assert
            Assert.ThrowsException<ArgumentException>(() =>
                new Teams().GetTeam("team"));
        }
        
        [TestMethod]
        public void NotThrowsNullExeptionWhenGetBoard()
        {
            // Arrange, Act, Assert
            Teams teams = new Teams();

            teams.AddTeam("team");
            teams.GetTeam("team").SetBoard(new Boards("board"));
            

            Assert.IsNotNull(teams.GetBoard("board"));
        }

        [TestMethod]
        public void ThrowsExeptionCheckIfPersonIsTeamMember()
        {
            Teams teams = new Teams();
            Assert.IsFalse(teams.CheckIfPersonIsTeamMember("personName"));
        }

        [TestMethod]
        public void NotThrowsExeptionCheckIfPersonIsTeamMember()
        {
            Teams teams = new Teams();
            teams.AddTeam("team");
            teams.GetTeam("team").AddMember(new Person("personName"));

            Assert.IsTrue(teams.CheckIfPersonIsTeamMember("personName"));
        }


    }
}

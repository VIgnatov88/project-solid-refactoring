﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using TeamWork_Project.Contracts;
using WorkItemManager.Entities;
using WorkItemManager.WorkIteams;
using System.Linq;

namespace Project.Tests.WorkItemsTests
{
    [TestClass]
    public class WorkItem_Should
    {
        [TestMethod]  // working
        public void ThrowExceptionWhen_WorkItemTitleIsNull()
        {
            var ex = Assert.ThrowsException<ArgumentException>(
               () => new Bug(null, "some valid description parameter"));

            Assert.AreEqual(ex.Message, "title cannot be null or empty!");
        }

        [TestMethod]  // working
        [DataRow("a")]
        [DataRow("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")]
        public void ThrowExceptionWhen_WorkItemTitleIs10to50(string invalidParam)
        {
            var ex = Assert.ThrowsException<ArgumentException>(
               () => new Bug(invalidParam, "some valid description parameter"));

            Assert.AreEqual(ex.Message, "Title must be between 10 and 50 characters!");
        }

        [TestMethod]  // working
        public void ThrowExceptionWhen_WorkItemDescriptionIsNull()
        {
            var ex = Assert.ThrowsException<ArgumentException>(
               () => new Bug("some valid title", null));

            Assert.AreEqual("description cannot be null or empty!", ex.Message);
        }

        [TestMethod]  // working
        [DataRow("a")]
        [DataRow(@"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")]
        public void ThrowExceptionWhen_WorkItemDescriptionIs10to500(string invalidParam)
        {
            var ex = Assert.ThrowsException<ArgumentException>(
               () => new Bug("some valid title", invalidParam));

            Assert.AreEqual("Description must be between 10 and 500 characters!", ex.Message);
        }

        [TestMethod]  // working
        public void ThrowExceptionWhen_SetPriorityOfStoryIsNotValid()
        {      
            var ex = Assert.ThrowsException<ArgumentException>(
               () => new Story().SetPriority("asd"));

            Assert.AreEqual("Cannot parse parameters", ex.Message);
        }

        [TestMethod]  // working
        [DataRow(0)]
        [DataRow(10)]
        public void ThrowExceptionWhen_CheckRatingOfFeedbackIsNotValid(int invalidParam)
        {
            var ex = Assert.ThrowsException<ArgumentException>(
               () => new Feedback().CheckRating(invalidParam));

            Assert.AreEqual("Your rating must be between 1 and 5 inclusive", ex.Message);
        }

        [TestMethod]  // working
        public void ThrowExceptionWhen_SetPriorityOfBugIsNotValid()
        {
            var ex = Assert.ThrowsException<ArgumentException>(
               () => new Bug().SetPriority("asd"));

            Assert.AreEqual("Cannot parse parameters", ex.Message);
        }

        [TestMethod]  // working
        public void ThrowExceptionWhen_SetSeverityOfBugIsNotValid()
        {
            var ex = Assert.ThrowsException<ArgumentException>(
               () => new Bug().SetSeverity("asd"));

            Assert.AreEqual("Cannot parse parameters", ex.Message);
        }



        [TestMethod]  // working
        public void PossibleAuthorsContain_ReturnsFalse_WhenInvalidPersonIsPassed()
        {
            var fakeWorkItem = new Bug();

            var fakePerson = new Mock<IPerson>();
            fakePerson.SetupGet(p => p.Name).Returns("Georgi");
            fakeWorkItem.PossibleAuthors.Add(fakePerson.Object);


            var fakePersonToAdd = new Mock<IPerson>();
            fakePersonToAdd.SetupGet(p => p.Name).Returns("Pesho");

            var sut = fakeWorkItem.PossibleAuthorsContain(fakePersonToAdd.Object);

            Assert.AreEqual(false, sut);
        }


        [TestMethod]  // working 
        public void AddComment_ThrowsException_WhenNonTeamMember_AddsComment()
        {
            var fakeWorkItem = new Bug();
            var fakePerson = new Mock<IPerson>();
            fakePerson.SetupGet(p => p.Name).Returns("Georgi");

            fakeWorkItem.PossibleAuthors.Add(fakePerson.Object);

            var fakePersonToAdd = new Mock<IPerson>();
            fakePersonToAdd.SetupGet(p => p.Name).Returns("Pesho");

            var ex = Assert.ThrowsException<ArgumentException>(
                () => fakeWorkItem.AddComment("some valid comment", fakePersonToAdd.Object));

            Assert.AreEqual("Only team members can leave comments!", ex.Message);
        }

        [TestMethod]  // working
        public void ChangeStatus_ThrowsException_WhenNewValue_IsNotValid()
        {
            var fakeWorkItem = new Bug();      

            var ex = Assert.ThrowsException<ArgumentException>(
                () => fakeWorkItem.ChangeStatus("invalidValue"));

            Assert.AreEqual("Argument is not a valid status value!", ex.Message);
        }

        [TestMethod]  // working
        [DataRow(null)]
        [DataRow("")]
        public void ChangeStatus_ThrowsException_WhenNewValue_IsNullOrEmpty(string invalidParams)
        {
            var fakeWorkItem = new Bug();

            var ex = Assert.ThrowsException<ArgumentException>(
                () => fakeWorkItem.ChangeStatus(invalidParams));

            Assert.AreEqual("Parameter cannot be null or empty!", ex.Message);
        }
    }

}

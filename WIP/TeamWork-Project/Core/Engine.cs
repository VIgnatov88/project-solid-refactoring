﻿
using System;
using TeamWork_Project.Contracts;
using WorkItemManager.Core.Contracts;
using WorkItemManager.DataBase;

namespace WorkItemManager.Core
{
    public class Engine : IRun
    {
        private string selectedOption;
        private IDataBase dataBase;
        private readonly ICommandParser commandParser;
        private readonly IWriter consoleWriter;
        private readonly IMenu menuSelect;
        private string output;

        public Engine(ICommandParser commandParser, IWriter consoleWriter, IMenu menuSelect, IDataBase dataBase)
        {
            this.commandParser = commandParser;
            this.consoleWriter = consoleWriter;
            this.menuSelect = menuSelect;
            this.dataBase = dataBase;
        }

        
        public virtual void Run()
        {
            while (true)
            {
                this.consoleWriter.PrintLogo();
                
                this.selectedOption = this.menuSelect.Menu();
                if (selectedOption.Equals("EXIT"))
                {
                    ConsoleResultOutput.Output(selectedOption, "Application is now closed");
                    break;
                }
                try
                {
                    var command = this.commandParser.ParseCommand(selectedOption);
                    this.output = command.Execute(dataBase, consoleWriter);
                }
                catch (Exception ex)
                {
                    output = $"ERROR: {ex.Message}";
                }

                ConsoleResultOutput.Output(selectedOption, output);
            }
        }
    }
}


﻿
using WorkItemManager.DataBase;

namespace WorkItemManager.Core.Contracts
{
    /// <summary>
    /// Inherited by all command objects
    /// Used to access the needed instance for the command
    /// </summary>
    /// <param name="dataBase"></param>
    /// <param name="consoleWriter"></param>
    /// <returns>Returns the result of the executed command as String</returns>
    public interface ICommand
    {
        string Execute(IDataBase dataBase, IWriter consoleWriter);
    }
}

﻿
namespace WorkItemManager.Core.Contracts
{
    /// <summary>
    /// Output: Menu
    /// Menu: Command options
    /// Return a command as String
    /// </summary>
    public interface IMenu
    {
        string Menu();
    }
}

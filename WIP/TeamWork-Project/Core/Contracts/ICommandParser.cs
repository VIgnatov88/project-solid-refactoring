﻿
namespace WorkItemManager.Core.Contracts
{
    /// <summary>
    /// Get the user input as String and search the assembly
    /// Parse the input String to ICommand Interface
    /// </summary>
    /// <param name="commandLine"></param>
    /// <returns>Returns ICommand from given String</returns>
    public interface ICommandParser
    {
        ICommand ParseCommand(string commandLine);
    }
}

﻿
namespace WorkItemManager.Core
{
    /// <summary>
    /// Used to generate Logo and the UI inputs
    /// </summary>
    public interface IWriter
    {
        void PrintLogo();

        string WriteLine(string line);
    }
}

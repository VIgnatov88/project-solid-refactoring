﻿
using Autofac;
using WorkItemManager.Core.Contracts;

namespace WorkItemManager.Core
{
    public class CommandParser : ICommandParser
    {
        private IComponentContext commandContext;

        public CommandParser(IComponentContext command)
        {
            this.commandContext = command;
        }

        public ICommand ParseCommand(string commandName)
        {
            return this.commandContext.ResolveNamed<ICommand>(commandName.ToLower());
        }
    }
}


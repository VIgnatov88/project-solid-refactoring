﻿
using System;

namespace WorkItemManager.Core
{
    public class ConsoleWriter : IWriter
    {
        public void PrintLogo()
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine(" -------------------------");
            Console.WriteLine(" |   Work Item Manager   |");
            Console.WriteLine(" -------------------------");
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        public string WriteLine(string line)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write(line + " ");
            Console.ForegroundColor = ConsoleColor.Gray;
            return Console.ReadLine();
        }
    }
}

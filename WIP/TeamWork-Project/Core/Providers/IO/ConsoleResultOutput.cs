﻿using System;

namespace WorkItemManager.Core
{
    public class ConsoleResultOutput
    {
        public static void Output(string selectedOption, string output)
        {
            Console.Clear();
            Console.WriteLine("\n************************************************************************");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write(" Last command: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine($"{selectedOption}");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(" Result: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine($"{output}");
            Console.WriteLine("************************************************************************");
        }
    }
}

﻿
using System;
using System.Collections.Generic;

namespace WorkItemManager
{
    public class IDProvider
    {
        private static List<int> IDs = new List<int>();

        public static int GetID()
        {
            Random rnd = new Random();
            int ID = rnd.Next(100, 1000);

            while (IDs.Contains(ID))
            {
                ID = rnd.Next(100, 1000);
            }
            IDs.Add(ID);
            return ID;
        }
    }
}

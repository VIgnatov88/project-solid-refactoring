﻿
using Ardoq.Consl;
using WorkItemManager.Core.Contracts;

namespace WorkItemManager.Core.Menu
{
    public class ConsoleMenu : IMenu
    {
        public string Menu()
        {
            var options = new[]
            {
                new ConsoleSelect.Option<string> {Key = "CreateNewPerson", Text = " Create new person", Selected = true},
                new ConsoleSelect.Option<string> {Key = "ShowAllPeople", Text = " Show all people"},
                new ConsoleSelect.Option<string> {Key = "ShowPersonActivity", Text = " Show person's activity"},
                new ConsoleSelect.Option<string> {Key = "CreateNewTeam", Text = " Create a new team"},
                new ConsoleSelect.Option<string> {Key = "ShowAllTeams", Text = " Show all teams"},
                new ConsoleSelect.Option<string> {Key = "ShowTeamActivity", Text = " Show Team's Activity"},
                new ConsoleSelect.Option<string> {Key = "AddPersonToTeam", Text = " Add Person To Team"},
                new ConsoleSelect.Option<string> {Key = "ShowAllTeamMembers", Text = " Show All Team Members"},
                new ConsoleSelect.Option<string> {Key = "CreateNewBoardInTeam", Text = " Create a New Board In a Team"},
                new ConsoleSelect.Option<string> {Key = "ShowAllTeamBoards", Text = " Show All Team Boards"},
                new ConsoleSelect.Option<string> {Key = "ShowBoardActivity", Text = " Show Board Activity"},
                new ConsoleSelect.Option<string> {Key = "CreateNewBugInBoard", Text = " Create new Bug in a board"},
                new ConsoleSelect.Option<string> {Key = "CreateNewStoryInBoard", Text = " Create new Story in a board"},
                new ConsoleSelect.Option<string> {Key = "CreateNewFeedbackInBoard", Text = " Create new Feedback in a board"},
                new ConsoleSelect.Option<string> {Key = "AssignWorkItemToPerson", Text = " Assign work item to a person"},
                new ConsoleSelect.Option<string> {Key = "UnassignWorkItemToPerson", Text = " Unassign work item to a person"},
                new ConsoleSelect.Option<string> {Key = "ListWorkItemsSortedByTitle", Text = " List work items sorted by title"},
                new ConsoleSelect.Option<string> {Key = "ListAllWorkItems", Text = " List all work items"},
                new ConsoleSelect.Option<string> {Key = "ListWorkItemsFilteredByBugs", Text = " List all work items filtered by Bug"},
                new ConsoleSelect.Option<string> {Key = "ListWorkItemsFilteredByStories", Text = " List all work items filtered by Story"},
                new ConsoleSelect.Option<string> {Key = "ListWorkItemsFilteredByFeedback", Text = " List all work items filtered by Feedback"},
                new ConsoleSelect.Option<string> {Key = "ChangePriorityOfBug", Text = " Change priority of Bug"},
                new ConsoleSelect.Option<string> {Key = "ChangePriorityOfStory", Text = " Change priority of Story"},
                new ConsoleSelect.Option<string> {Key = "ChangeStatusOfStory", Text = " Change status of Story"},
                new ConsoleSelect.Option<string> {Key = "ChangeStatusOfBug", Text = " Change status of Bug"},
                new ConsoleSelect.Option<string> {Key = "ChangeStatusOfFeedback", Text = " Change status of Feedback"},
                new ConsoleSelect.Option<string> {Key = "ChangeRatingOfFeedback", Text = " Change rating of Feedback"},
                new ConsoleSelect.Option<string> {Key = "AddCommentToWorkItem", Text = " Add comment to Work item"},
                new ConsoleSelect.Option<string> {Key = "ListWorkItemsFilteredByStatus", Text = " List all work items filtered by Status"},


                new ConsoleSelect.Option<string> {Key = "EXIT", Text = " EXIT"}
            };
            return new ConsoleSelect().PromptSelection("   Select an option:", options);
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkItemManager.Entities;

namespace TeamWork_Project.Contracts
{
    public interface IAssignable : IWorkItem
    {
        IPerson Assignee { get; }
        void SetAssignee(IPerson tryAssignee);
        void UnassignPerson();
    }
}

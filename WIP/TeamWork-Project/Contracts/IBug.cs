﻿
using System.Collections.Generic;
using TeamWork_Project.Common;
using WorkItemManager.Entities;

namespace TeamWork_Project.Contracts
{
    public interface IBug : IPrioritizable, IWorkItem
    {
        List<string> StepsToReproduce { get; set; }
        Severity Severity { get; set; }
        void ChangeStatus();
    }
}

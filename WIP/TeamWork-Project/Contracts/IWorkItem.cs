﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamWork_Project.Common;
using WorkItemManager.Entities;
using WorkItemManager.WorkIteams;

namespace TeamWork_Project.Contracts
{
    public interface IWorkItem
    {
        int ID { get; }
        string Title { get; set; }
        string Description { get; set; }
        List<Comment> Comments { get; }
        List<IPerson> PossibleAuthors { get; }
        List<History> History { get; }
        Status Status { get; }

        bool PossibleAuthorsContain(IPerson tryAuthor);
        void AddComment(string message, IPerson author);
        void AddToHistory(string activity);
        void ChangeStatus(string newValue);
        void SetPossibleAuthors(List<IPerson> authors);
    }
}

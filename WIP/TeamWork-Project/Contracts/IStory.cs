﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamWork_Project.Common;
using WorkItemManager.Entities;

namespace TeamWork_Project.Contracts
{
    interface IStory : IWorkItem
    {
        Priority Priority { get; set; }
        Size Size { get; set; }
    }
}

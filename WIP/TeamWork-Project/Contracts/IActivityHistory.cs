﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkItemManager.WorkIteams;

namespace TeamWork_Project.Contracts
{
    public interface IActivityHistory 
    {
        IList<string> ActHistory { get; set; }

        void ShowActivityHistory();
   
    }
}

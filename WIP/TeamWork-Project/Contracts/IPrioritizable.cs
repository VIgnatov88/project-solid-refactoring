﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamWork_Project.Common;

namespace TeamWork_Project.Contracts
{
    public interface IPrioritizable : IWorkItem
    {
        Priority Priority { get; set; }
        Priority SetPriority(string tryPriority);
    }
}

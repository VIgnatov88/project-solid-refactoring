﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkItemManager.WorkIteams;

namespace TeamWork_Project.Contracts
{
    public interface IPerson
    {
        string Name { get;}
        List<History> ActivityHistory { get;  }
        List<IWorkItem> WorkItems { get; }
        void RemoveWorkItem(string itemName);
        string ListHistory();
        bool CheckIfWorkItemExists(string tryTitle);
        IWorkItem GetWorkItem(string tryTitle);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkItemManager.WorkIteams;

namespace TeamWork_Project.Contracts
{
    public interface IBoard
    {
        string Name { get; set; }
        List<IWorkItem> WorkItems { get; set; }
        List<History> ActivityHistory { get; set; }
        bool WorkItemExistsInBoard(string workItemName, string tryType);
        void AddWorkitemToBoard(IWorkItem newItem);
        IWorkItem GetWorkItemFromBoard(string title, string tryType);
        string ListWorkitemsFilteredByStory();
        string ListWorkitemsFilteredByBug();
        string ListWorkitemsFilteredByFeedback();
        void AddToHistory(string message);
        string ListAllWorkitemsInBoard();
        string ListWorkitemsFilteredByStatus(string tryValue);
        string ListWorkitemsByTitle();
        IWorkItem FindWorkItemByTitle(string tryTitle, string tryType);
        string ShowBoardActivity();
    }
}

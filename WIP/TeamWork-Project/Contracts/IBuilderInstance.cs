﻿
namespace TeamWork_Project.Contracts
{
    public interface IBuilderInstance
    {
        void BuildApp();
    }
}
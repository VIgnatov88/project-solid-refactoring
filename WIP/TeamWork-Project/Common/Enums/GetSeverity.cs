﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamWork_Project.Common
{
    public enum Severity
    {
        Critical, Major, Minor
    }
}
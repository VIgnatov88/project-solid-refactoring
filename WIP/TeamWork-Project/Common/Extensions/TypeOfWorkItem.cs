﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamWork_Project.Contracts;
using WorkItemManager.WorkIteams;

namespace TeamWork_Project.Common.Extensions
{
    public static class TypeOfWorkItem
    {
        public static IWorkItem GetWorkItemType(string tryType)
        {
            switch (tryType.ToLower())
            {
                case "bug": return new Bug(); ;

                case "feedback": return new Feedback();

                default: return new Story();
            }
        }
    }
}

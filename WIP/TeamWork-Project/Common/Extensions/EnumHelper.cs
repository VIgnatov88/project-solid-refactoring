﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamWork_Project.Common
{ 
    public static class EnumHelper
    {
        //public static Enum ConvertToEnum(this string tryValue)
        //{
        //    if (string.IsNullOrEmpty(tryValue))
        //    {
        //        throw new ArgumentException("Parameter cannot be null or empty!");
        //    }

        //    GetStatus newStatus;
        //    if (Enum.TryParse<GetStatus>(tryValue, out newStatus))
        //    {
        //        return newStatus;
        //    }

        //    GetPriority newPriority;
        //    if (Enum.TryParse<GetPriority>(tryValue, out newPriority))
        //    {
        //        return newPriority;
        //    }

        //    GetSeverity newSeverity;
        //    if (Enum.TryParse<GetSeverity>(tryValue, out newSeverity))
        //    {
        //        return newSeverity;
        //    }

        //    GetSize newSize;
        //    if (Enum.TryParse<GetSize>(tryValue, out newSize))
        //    {
        //        return newSize;
        //    }
        //    throw new InvalidCastException("Cannot cast parameter!");
        //}

        public static Status ToEnumStatus(this string tryValue)
        {
            if (string.IsNullOrEmpty(tryValue))
            {
                throw new ArgumentException("Parameter cannot be null or empty!");
            }
            if (!Enum.TryParse(tryValue, true, out Status newStatus))
            {
                throw new ArgumentException("Argument is not a valid status value!");
            }

            return newStatus;
        }

        public static Severity ToEnumSeverity(this string tryValue)
        {
            if (string.IsNullOrEmpty(tryValue))
            {
                throw new ArgumentException("Parameter cannot be null or empty!");
            }
            if (!Enum.TryParse(tryValue, out Severity newSeverity))
            {
                throw new ArgumentException("Argument is not a valid status value!");
            }

            return newSeverity;
        }

        public static Priority ToEnumPriority(this string tryValue)
        {
            if (string.IsNullOrEmpty(tryValue))
            {
                throw new ArgumentException("Parameter cannot be null or empty!");
            }
            if (!Enum.TryParse(tryValue, out Priority newPriority))
            {
                throw new ArgumentException("Argument is not a valid status value!");
            }
            return newPriority;
        }

        public static Size ToEnumSize(this string tryValue)
        {
            if (string.IsNullOrEmpty(tryValue))
            {
                throw new ArgumentException("Parameter cannot be null or empty!");
            }
            if (!Enum.TryParse(tryValue, out Size newSize))
            {
                throw new ArgumentException("Argument is not a valid status value!");
            }
            return newSize;
        }
    }
}

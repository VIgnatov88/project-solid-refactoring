﻿
namespace TeamWork_Project.Core.Commands
{
    using WorkItemManager.Core;
    using WorkItemManager.Core.Contracts;
    using WorkItemManager.DataBase;

    public class ListWorkItemsSortedByTitle : ICommand
    {
        //TODO: finish command

        public string Execute(IDataBase dataBase, IWriter Writer)
        {
            string output = dataBase.Teams.ListAllExistingWorkitemsInTeamByTitle();

            return $"All workitems sorted by title:\n{output}";
        }
    }
}

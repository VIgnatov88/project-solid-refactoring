﻿
namespace TeamWork_Project.Core.Commands
{
    using System;
    using WorkItemManager.Core;
    using WorkItemManager.Core.Contracts;
    using WorkItemManager.DataBase;

    public class ChangeSeverityOfBug : ICommand
    {
        public string Execute(IDataBase dataBase, IWriter Writer)
        {
            string workitemTitle = Writer.WriteLine("Bug's title: ");
            string newSeverity = Writer.WriteLine("Enter the new severity value: ");
            /*
            if (!Instances.WorkItems.ContainsKey(workitemTitle))
            {
                throw new ArgumentException("Workitem with such title does not exist!");
            }

            else if (Instances.WorkItems.ContainsKey(workitemTitle) && Instances.WorkItems[workitemTitle].GetType().Name.ToLower() != "bug")
            {
                throw new ArgumentException("Only non-bug workitems with such title exist");
            }

            Bug bug = (Bug)Instances.WorkItems[workitemTitle];
            bug.SetSeverity(newSeverity);
            string activity =
                $" Severity of {workitemTitle} was changed to {newSeverity} " +
                $"at { DateTime.Now.ToString("HH:mm:ss")} " +
                $"on { DateTime.Now.ToString("dd/MM/yyyy")}!\n";

            bug.AddToHistory(activity);

            return activity;
            */
            throw new NotImplementedException();
        }
    }
}

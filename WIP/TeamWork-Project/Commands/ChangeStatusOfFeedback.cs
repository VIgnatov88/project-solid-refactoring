﻿
namespace TeamWork_Project.Core.Commands
{
    using System;
    using WorkItemManager.Core;
    using WorkItemManager.Core.Contracts;
    using WorkItemManager.DataBase;

    public class ChangeStatusOfFeedback : ICommand
    {
        public string Execute(IDataBase dataBase, IWriter Writer)
        {
            string workitemTitle = Writer.WriteLine("Feedback's title: ");
            string newStatus = Writer.WriteLine("Enter the new status value: ");

            if (dataBase.Teams.GetWorkItem(workitemTitle, "feedback") != null)
            {
                var feedback = dataBase.Teams.GetWorkItem(workitemTitle, "feedback");

                feedback.ChangeStatus(newStatus);
                string activity =
                    $" Status of {workitemTitle} was changed to {feedback.Status.ToString()} " +
                    $"at { DateTime.Now.ToString("HH:mm:ss")} " +
                    $"on { DateTime.Now.ToString("dd/MM/yyyy")}!";

                feedback.AddToHistory(activity);

                return activity;
            }
            throw new ArgumentException("Workitem of type feedback with such title does not exist!");
        }
    }
}

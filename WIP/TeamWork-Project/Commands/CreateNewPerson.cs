﻿
namespace WorkItemManager.Core.Commands
{
    using System;
    using WorkItemManager.Core.Contracts;
    using WorkItemManager.DataBase;
    using WorkItemManager.Entities;
    using WorkItemManager.WorkIteams;

    public class CreateNewPerson : ICommand
    {
        public string Execute(IDataBase dataBase, IWriter Writer)
        {
            string newPersonName = Writer.WriteLine("Person's name: ");
            
            Person person = new Person(newPersonName);
            string activity = $" - Person with name: {newPersonName}, " +
                              $"was created at { DateTime.Now.ToString("HH:mm:ss")} " +
                              $"on { DateTime.Now.ToString("dd/MM/yyyy")}!";

            person.ActivityHistory.Add(new History(activity));
            
            dataBase.Members.AddPerson(person);
            return activity;
        }
    }
}


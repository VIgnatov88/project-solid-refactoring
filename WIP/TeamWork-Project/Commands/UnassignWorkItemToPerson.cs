﻿
namespace TeamWork_Project.Core.Commands
{
    using Ardoq.Consl;
    using System;
    using System.Linq;
    using System.Reflection;
    using TeamWork_Project.Contracts;
    using WorkItemManager.Core;
    using WorkItemManager.Core.Contracts;
    using WorkItemManager.DataBase;
    using WorkItemManager.WorkIteams;

    public class UnassignWorkItemToPerson : ICommand
    {
        public string Execute(IDataBase dataBase, IWriter Writer)
        {
            ConsoleSelect consoleSelect = new ConsoleSelect();

            string name = Writer.WriteLine("Person's name: ");

            if(!dataBase.Members.ContainsMember(name))
                throw new ArgumentException("only members of teams can be assigned to a workitem!");

            var person = dataBase.Members.GetPerson(name);

            var subMenuOptions = new[]
            {
                new ConsoleSelect.Option<string> { Key = "Bug", Text = "   - Bug", Selected = true },
                new ConsoleSelect.Option<string> { Key = "Story", Text = "   - Story" },
            };
            string selectedWorkItem = consoleSelect.PromptSelection("   Select an option:", subMenuOptions);

            subMenuOptions = new[]
            {
                new ConsoleSelect.Option<string> { Key = "New", Text = "    - New", Selected = true },
                new ConsoleSelect.Option<string> { Key = "Existing", Text = "    - Existing" }
            };
            string workItemType = consoleSelect.PromptSelection("   Select an option:", subMenuOptions);

            string title = Writer.WriteLine($"enter the title of the {selectedWorkItem}: ");


            if (workItemType.ToLower().Equals("new"))
            {
                Assembly commandAssembly = Assembly.GetAssembly(typeof(ICommand));

                var commandType = commandAssembly.DefinedTypes
                    .Where(x => x.ImplementedInterfaces.Any(y => y == typeof(ICommand)))
                    .Where(x => !x.IsAbstract)
                    .SingleOrDefault(x => x.Name.ToLower() == (selectedWorkItem.ToLower()));

                IAssignable workItem = Activator.CreateInstance(commandType) as IAssignable;

                workItem.Title = title;
               
                if (dataBase.Teams.GetWorkItemByTitleAndType(title, workItemType) != null )
                   throw new ArgumentException($"a workitem of type {workItemType} with such title already exists!");


                string description = Writer.WriteLine($"describe the {selectedWorkItem}: ");
                workItem.Description = description;
                dataBase.Members.GetPerson(name).WorkItems.Add(workItem);
            }

            else
            {
                //var boardlist = Instances.BoardsInstance.Values;
                //bool itemExists = false;

                IAssignable assignableItem = (IAssignable)dataBase.Teams.GetWorkItemByTitleAndType(title, workItemType);
                assignableItem.SetAssignee(person);

                //if (Instances.WorkItemsInstance.ContainsKey(title))
                //    itemExists = true;

                //if (!itemExists)
                //{
                //    throw new ArgumentException($"workitem of type {selectedWorkItem} with such title doest not exist!");
                //}

                //IWorkItem typeItem = Instances.WorkItemsInstance[title];
                //Instances.MembersInstance.GetPerson(name).WorkItems.Add(typeItem);
            }

            string activity =
                $"a workitem of type {selectedWorkItem} was assigned to person \"{name}\" " +
                $"at { DateTime.Now.ToString("hh:mm:ss")} " +
                $"on { DateTime.Now.ToString("dd/mm/yyyy")}!";

            person.ActivityHistory.Add(new History(activity));

            return activity;
        }
    }
}



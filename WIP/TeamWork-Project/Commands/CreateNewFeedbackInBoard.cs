﻿
namespace TeamWork_Project.Core.Commands
{
    using System;
    using TeamWork_Project.Common.Extensions;
    using TeamWork_Project.Contracts;
    using WorkItemManager.DataBase;
    using WorkItemManager.Core;
    using WorkItemManager.Core.Contracts;

    public class CreateNewFeedbackInBoard : ICommand
    {
        public string Execute(IDataBase dataBase, IWriter Writer)
        {
            string boardName = Writer.WriteLine("Board's name: ");
            var currentBoard = dataBase.Teams.GetBoard(boardName);
            string feedbackTitle = Writer.WriteLine("Feedback's title: ");
            IWorkItem item = TypeOfWorkItem.GetWorkItemType("feedback");
            item.Title = feedbackTitle;
            currentBoard.AddWorkitemToBoard(item);

            string activity = $" - Workitem of type feedback with title: {feedbackTitle}, was added to board: {boardName} " +
                              $"at { DateTime.Now.ToString("HH:mm:ss")} " +
                              $"on { DateTime.Now.ToString("dd/MM/yyyy")}!";

            item.SetPossibleAuthors(dataBase.Teams.GetPossibleAuthors(currentBoard.Name));
            currentBoard.AddToHistory(activity);
            return activity;
        }
    }
}


﻿
namespace WorkItemManager.Core.Commands
{
    using WorkItemManager.Core.Contracts;
    using WorkItemManager.DataBase;

    public class ShowAllTeams : ICommand
    {
        public string Execute(IDataBase dataBase, IWriter Writer)
        {
            return dataBase.Teams.ShowAllTeams();
        }
    }
}
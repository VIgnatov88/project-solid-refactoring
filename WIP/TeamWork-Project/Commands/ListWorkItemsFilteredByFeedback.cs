﻿
namespace TeamWork_Project.Core.Commands
{
    using WorkItemManager.Core;
    using WorkItemManager.Core.Contracts;
    using WorkItemManager.DataBase;

    public class ListWorkItemsFilteredByFeedback : ICommand
    {
        public string Execute(IDataBase dataBase, IWriter Writer)
        {
            return dataBase.Teams.ListAllExistingWorkitemsInTeamFilteredByFeedback();

        }
    }
}

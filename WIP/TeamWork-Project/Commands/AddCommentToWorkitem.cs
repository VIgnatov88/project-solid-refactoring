﻿
namespace TeamWork_Project.Core.Commands
{
    using System;
    using WorkItemManager.Core;
    using WorkItemManager.Core.Contracts;
    using WorkItemManager.DataBase;
    using WorkItemManager.WorkIteams;

    public class AddCommentToWorkitem : ICommand
    {
        public string Execute(IDataBase dataBase, IWriter Writer)
        {
            string workitemTitle = Writer.WriteLine("WorkItem's title: ");
            string workitemType = Writer.WriteLine("WorkItem's type: ");

            var workitem = dataBase.Teams.GetWorkItemByTitleAndType(workitemTitle, workitemType);
            string personName = Writer.WriteLine("Enter your name: ");

            string comment = Writer.WriteLine("Leave your comment: ");
            string activity =
                $" - Comment \"{comment}\" was added to WorkItem with title: {workitemTitle} " +
                $"at { DateTime.Now.ToString("HH:mm:ss")} " +
                $"on { DateTime.Now.ToString("dd/MM/yyyy")}!";


            var currentPerson = dataBase.Members.GetPerson(personName);
            workitem.AddComment(comment, currentPerson);
            currentPerson.ActivityHistory.Add(new History(activity));
            
            return activity;
        }
    }
}

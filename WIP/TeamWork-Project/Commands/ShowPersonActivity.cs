﻿
namespace WorkItemManager.Core.Commands
{
    using TeamWork_Project.Contracts;
    using WorkItemManager.Core.Contracts;
    using WorkItemManager.DataBase;
    using WorkItemManager.Entities;

    public class ShowPersonActivity : ICommand
    {
        public string Execute(IDataBase dataBase, IWriter Writer)
        {
            string personName = Writer.WriteLine("Person's name: ");
            IPerson currentPerson = dataBase.Members.GetPerson(personName);
            return currentPerson.ListHistory();
        }
    }
}
﻿
namespace TeamWork_Project.Core.Commands
{
    using WorkItemManager.Core;
    using WorkItemManager.DataBase;
    using WorkItemManager.Core.Contracts;

    public class ListAllWorkItems : ICommand
    {
        public string Execute(IDataBase dataBase, IWriter consoleWriter)
        {
            string output = dataBase.Teams.ListAllExistingWorkitems();

            return $"All existing workitems:\n{output}";
        }
    }
}

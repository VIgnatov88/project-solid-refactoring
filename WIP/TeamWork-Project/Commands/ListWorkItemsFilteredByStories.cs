﻿
namespace TeamWork_Project.Core.Commands
{
    using WorkItemManager.Core;
    using WorkItemManager.Core.Contracts;
    using WorkItemManager.DataBase;

    public class ListWorkItemsFilteredByStories : ICommand
    {
        public string Execute(IDataBase dataBase, IWriter Writer)
        {
            return dataBase.Teams.ListAllExistingWorkitemsInTeamFilteredByStory();
        }
    }
}

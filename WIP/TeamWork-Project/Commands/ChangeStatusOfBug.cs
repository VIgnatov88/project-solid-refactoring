﻿
namespace TeamWork_Project.Core.Commands
{
    using System;
    using WorkItemManager.Core;
    using WorkItemManager.Core.Contracts;
    using WorkItemManager.DataBase;
    using WorkItemManager.WorkIteams;

    public class ChangeStatusOfBug : ICommand
    {
        public string Execute(IDataBase dataBase, IWriter Writer)
        {
            string workitemTitle = Writer.WriteLine("Bug's title: ");

            if (dataBase.Teams.GetWorkItem(workitemTitle, "bug") != null)
            {
                var workItem = dataBase.Teams.GetWorkItem(workitemTitle, "bug");
                var bug = workItem as Bug;
                var oldStatus = bug.Status.ToString();
                bug.ChangeStatus();
                string activity =
                    $" Status of {workitemTitle} was changed from {oldStatus} to {bug.Status.ToString()} " +
                    $"at { DateTime.Now.ToString("HH:mm:ss")} " +
                    $"on { DateTime.Now.ToString("dd/MM/yyyy")}!";

                bug.AddToHistory(activity);

                return activity;
            }
            throw new ArgumentException("Workitem of type bug with such title does not exist!");
        }
    }
}

﻿
namespace TeamWork_Project.Core.Commands
{
    using System;
    using TeamWork_Project.Contracts;
    using WorkItemManager.Core;
    using WorkItemManager.DataBase;
    using WorkItemManager.Core.Contracts;

    public class ChangePriorityOfStory : ICommand
    {
        public string Execute(IDataBase dataBase, IWriter Writer)
        {
            string workitemTitle = Writer.WriteLine("Story's title: ");
            string newPriority = Writer.WriteLine("Enter the new priority value: ");

            if (dataBase.Teams.GetWorkItem(workitemTitle, "story") == null)
            {
                throw new ArgumentException("Workitem of type story with such title does not exist!");
            }

            IWorkItem workItem = dataBase.Teams.GetWorkItem(workitemTitle, "story");
            var story = workItem as IPrioritizable;
            story.SetPriority(newPriority.ToLower());
            string activity =
                $" Priority of {workitemTitle} was changed to {story.Priority.ToString()} " +
                $"at { DateTime.Now.ToString("HH:mm:ss")} " +
                $"on { DateTime.Now.ToString("dd/MM/yyyy")}!";

            story.AddToHistory(activity);

            return activity;
        }
    }
}

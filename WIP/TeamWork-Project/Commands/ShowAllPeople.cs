﻿
namespace WorkItemManager.Core.Commands
{
    using WorkItemManager.Core.Contracts;
    using WorkItemManager.DataBase;

    public class ShowAllPeople : ICommand
    {
        public string Execute(IDataBase dataBase, IWriter Writer)
        {
            return dataBase.Members.ShowAllPeople();
        }
    }
}
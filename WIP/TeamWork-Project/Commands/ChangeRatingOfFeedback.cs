﻿
namespace TeamWork_Project.Core.Commands
{
    using System;
    using TeamWork_Project.Contracts;
    using WorkItemManager.Core;
    using WorkItemManager.Core.Contracts;
    using WorkItemManager.DataBase;
    using WorkItemManager.WorkIteams;

    public class ChangeRatingOfFeedback : ICommand
    {
        public string Execute(IDataBase dataBase, IWriter Writer)
        {
            string workitemTitle = Writer.WriteLine($"Enter the title of the Feedback: ");
            string newRating = Writer.WriteLine($"Enter the new rating value: ");

            if (dataBase.Teams.GetWorkItem(workitemTitle, "feedback") == null)
            {
                throw new ArgumentException("Workitem of type feedback with such title does not exist!");
            }

            IWorkItem item = dataBase.Teams.GetWorkItem(workitemTitle, "feedback");
            var feedback = item as Feedback;
            string oldRating = feedback.Rating.ToString();
            feedback.CheckRating(int.Parse(newRating));

            string activity =
                $" Rating of feedback with title {workitemTitle} was changed from {oldRating} to {newRating} " +
                $"at { DateTime.Now.ToString("HH:mm:ss")} " +
                $"on { DateTime.Now.ToString("dd/MM/yyyy")}!";

            feedback.AddToHistory(activity);

            return activity;
        }
    }
}

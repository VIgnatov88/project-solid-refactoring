﻿
namespace TeamWork_Project.Core.Commands
{
    using WorkItemManager.Core;
    using WorkItemManager.Core.Contracts;
    using WorkItemManager.DataBase;

    public class ShowBoardActivity : ICommand
    {
        public string Execute(IDataBase dataBase, IWriter Writer)
        {
            string boardName = Writer.WriteLine("Board's name: ");
            return //$"Board: { boardName}:" +
                   $"{dataBase.Teams.ShowBoardActivity(boardName)}";
        }
    }
}

﻿
namespace TeamWork_Project.Core.Commands
{
    using System;
    using WorkItemManager.Core;
    using WorkItemManager.Core.Contracts;
    using WorkItemManager.DataBase;
    using WorkItemManager.Entities;
    using WorkItemManager.WorkIteams;

    public class CreateNewBoardInTeam : ICommand
    {
        public string Execute(IDataBase dataBase, IWriter Writer)
        {
            string teamName = Writer.WriteLine("Team's name: ");
            var team = dataBase.Teams.GetTeam(teamName);
            string boardName = Writer.WriteLine("Board's name: ");
            var board = new Boards(boardName);
            team.SetBoard(board);
            
            string activity = $" - Board: {boardName} was added to team: {teamName} " +
                              $"at { DateTime.Now.ToString("HH:mm:ss")} " +
                              $"on { DateTime.Now.ToString("dd/MM/yyyy")}!";

            dataBase.Boards.ActivityHistory.Add(new History(activity));
            team.ActivityHistory.Add(new History(activity));
            team.AddToBoardHistory(board, activity);
            return activity;
        }
    }
}

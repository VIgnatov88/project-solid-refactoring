﻿
namespace TeamWork_Project.Core.Commands
{
    using WorkItemManager.Core;
    using WorkItemManager.Core.Contracts;
    using WorkItemManager.DataBase;

    public class ListWorkItemsFilteredByStatus : ICommand
    {
        public string Execute(IDataBase dataBase, IWriter consoleWriter)
        {
            string tryValue = consoleWriter.WriteLine("Please enter the status you want to filter the workitems by: ");

            string output = dataBase.Teams.ListAllExistingWorkitemsInTeamFilteredByStatus(tryValue);

            return $"{output}";
        }
    }
}

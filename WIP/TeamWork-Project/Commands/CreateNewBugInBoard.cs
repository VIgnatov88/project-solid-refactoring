﻿
namespace TeamWork_Project.Core.Commands
{
    using System;
    using TeamWork_Project.Common.Extensions;
    using TeamWork_Project.Contracts;
    using WorkItemManager.Core;
    using WorkItemManager.Core.Contracts;
    using WorkItemManager.DataBase;

    public class CreateNewBugInBoard : ICommand
    {
        public string Execute(IDataBase dabaBase, IWriter Writer)
        {
            string boardName = Writer.WriteLine("Board's name: ");
            var currentBoard = dabaBase.Teams.GetBoard(boardName);
            string bugTitle = Writer.WriteLine("Bug's title: ");
            IWorkItem item = TypeOfWorkItem.GetWorkItemType("bug");
            item.Title = bugTitle;
            currentBoard.AddWorkitemToBoard(item);

            string activity = $" - Workitem of type bug with title: {bugTitle} was added to board: {boardName} " +
                              $"at { DateTime.Now.ToString("HH:mm:ss")} " +
                              $"on { DateTime.Now.ToString("dd/MM/yyyy")}!";

            item.SetPossibleAuthors(dabaBase.Teams.GetPossibleAuthors(currentBoard.Name));
            currentBoard.AddToHistory(activity);
            return activity;
        }
    }
}

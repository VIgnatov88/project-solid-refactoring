﻿
namespace TeamWork_Project.Core.Commands
{
    using System;
    using WorkItemManager.Core;
    using WorkItemManager.Core.Contracts;
    using WorkItemManager.DataBase;

    public class ChangeStatusOfStory : ICommand
    {
        public string Execute(IDataBase dataBase, IWriter Writer)
        {
            string workitemTitle = Writer.WriteLine("Story's title: ");
            string newStatus = Writer.WriteLine("Enter the new status value: ");

            if (dataBase.Teams.GetWorkItem(workitemTitle, "story") != null)
            {
                var story = dataBase.Teams.GetWorkItem(workitemTitle, "story");

                story.ChangeStatus(newStatus);
                string activity =
                    $" Status of {workitemTitle} was changed to {story.Status.ToString()} " +
                    $"at { DateTime.Now.ToString("HH:mm:ss")} " +
                    $"on { DateTime.Now.ToString("dd/MM/yyyy")}!";

                story.AddToHistory(activity);

                return activity;
            }
            throw new ArgumentException("Workitem of type story with such title does not exist!");
        }
    }
}

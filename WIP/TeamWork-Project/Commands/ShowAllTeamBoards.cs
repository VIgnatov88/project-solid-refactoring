﻿
namespace TeamWork_Project.Core.Commands
{
    using WorkItemManager.Core;
    using WorkItemManager.Core.Contracts;
    using WorkItemManager.DataBase;

    public class ShowAllTeamBoards : ICommand
    {
        public string Execute(IDataBase dataBase, IWriter Writer)
        {
            string teamName = Writer.WriteLine("Team's name: ");
            return dataBase.Teams.GetTeam(teamName).GetAllBoards();
        }
    }
}

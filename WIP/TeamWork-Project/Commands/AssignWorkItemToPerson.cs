﻿
namespace TeamWork_Project.Core.Commands
{
    using Ardoq.Consl;
    using System;
    using System.Linq;
    using System.Reflection;
    using WorkItemManager.DataBase;
    using TeamWork_Project.Common.Extensions;
    using TeamWork_Project.Contracts;
    using WorkItemManager.Core;
    using WorkItemManager.Core.Contracts;
    using WorkItemManager.WorkIteams;

    public class AssignWorkItemToPerson : ICommand
    {
        public string Execute(IDataBase dataBase, IWriter Writer)
        {
            ConsoleSelect consoleSelect = new ConsoleSelect();

            string name = Writer.WriteLine("person's name: ");

            if (dataBase.Teams.CheckIfPersonIsTeamMember(name))
                throw new ArgumentException("only members of teams can be assigned to a workitem!");


            var subMenuOptions = new[]
            {
                    new ConsoleSelect.Option<string> { Key = "Bug", Text = "   - Bug", Selected = true },
                    new ConsoleSelect.Option<string> { Key = "Story", Text = "   - Story" },
                };
            string selectedWorkItem = consoleSelect.PromptSelection("   Select an option:", subMenuOptions);

            subMenuOptions = new[]
            {
                    new ConsoleSelect.Option<string> { Key = "New", Text = "    - New", Selected = true },
                    new ConsoleSelect.Option<string> { Key = "Existing", Text = "    - Existing" }
                };
            string workItemType = consoleSelect.PromptSelection("   Select an option:", subMenuOptions);

            string title = Writer.WriteLine($"enter the title of the {selectedWorkItem}: ");
            var currentPerson = dataBase.Members.GetPerson(name);

            if (workItemType.ToLower().Equals("new"))
            {
                Assembly commandAssembly = Assembly.GetAssembly(typeof(ICommand));
                
                var commandType = commandAssembly.DefinedTypes
                    .Where(x => x.ImplementedInterfaces.Any(y => y == typeof(ICommand)))
                    .Where(x => !x.IsAbstract)
                    .SingleOrDefault(x => x.Name.ToLower() == (selectedWorkItem.ToLower()));

                var workItem = Activator.CreateInstance(commandType) as WorkItem;

                workItem.Title = title;
                if (!dataBase.Boards.WorkItemExistsInBoard(title, workItem.GetType().Name))
                    throw new ArgumentException($"a workitem of type {workItemType} with such title already exists!");


                string description = Writer.WriteLine($"describe the {selectedWorkItem}: ");
                workItem.Description = description;
                dataBase.Members.GetPerson(name).WorkItems.Add(workItem);
            }

            else
            { 

                if (dataBase.Teams.GetWorkItemByTitleAndType(title, selectedWorkItem) == null)
                    throw new ArgumentException($"workitem of type {selectedWorkItem} with such title doest not exist!");

                IWorkItem typeItem = TypeOfWorkItem.GetWorkItemType(selectedWorkItem);               
                currentPerson.WorkItems.Add(typeItem);
            }

            string activity =
                $" A workitem of type {selectedWorkItem} was assigned to person \"{name}\" " +
                $"at { DateTime.Now.ToString("hh:mm:ss")} " +
                $"on { DateTime.Now.ToString("dd/mm/yyyy")}!";
            currentPerson.ActivityHistory.Add(new History(activity));
            return activity;
        }
    }
}



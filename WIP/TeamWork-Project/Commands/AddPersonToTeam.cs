﻿
namespace TeamWork_Project.Core.Commands
{
    using System;
    using WorkItemManager.Core;
    using WorkItemManager.Core.Contracts;
    using WorkItemManager.DataBase;
    using WorkItemManager.WorkIteams;

    public class AddPersonToTeam : ICommand
    {
        public string Execute(IDataBase dataBase, IWriter Writer)
        {
            string personName = Writer.WriteLine("Person's name: ");
            string teamName = Writer.WriteLine("Team's name: ");
            string activity =
                $" - {personName} was added to team {teamName} " +
                $"at { DateTime.Now.ToString("HH:mm:ss")} " +
                $"on { DateTime.Now.ToString("dd/MM/yyyy")}!";
            
            var newTeam = dataBase.Teams.GetTeam(teamName);
            var newPerson = dataBase.Members.GetPerson(personName);
            newPerson.ActivityHistory.Add(new History(activity));
            newTeam.ActivityHistory.Add(new History(activity));
            newTeam.AddMember(newPerson);

            return activity;
        }
    }
}

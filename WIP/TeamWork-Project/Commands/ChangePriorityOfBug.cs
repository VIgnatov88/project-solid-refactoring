﻿
namespace TeamWork_Project.Core.Commands
{
    using System;
    using TeamWork_Project.Contracts;
    using WorkItemManager.Core;
    using WorkItemManager.Core.Contracts;
    using WorkItemManager.DataBase;

    public class ChangePriorityOfBug : ICommand
    {
        public string Execute(IDataBase dataBase, IWriter Writer)
        {
            string workitemTitle = Writer.WriteLine("Bug's title: ");
            string newPriority = Writer.WriteLine("Enter the new priority value: ");

            if (dataBase.Teams.GetWorkItem(workitemTitle, "bug") != null)
            {
                IWorkItem workItem = dataBase.Teams.GetWorkItem(workitemTitle, "bug");
                var bug = workItem as IPrioritizable;
                bug.SetPriority(newPriority);
                string activity =
                    $" Priority of {workitemTitle} was changed to {bug.Priority.ToString()} " +
                    $"at { DateTime.Now.ToString("HH:mm:ss")} " +
                    $"on { DateTime.Now.ToString("dd/MM/yyyy")}!";

                bug.AddToHistory(activity);

                return activity;
            }
                throw new ArgumentException("Workitem of type bug with such title does not exist!");

        }
    }
}

﻿
namespace TeamWork_Project.Core.Commands
{
    using System;
    using WorkItemManager.Core;
    using WorkItemManager.DataBase;
    using WorkItemManager.Core.Contracts;

    public class CreateNewTeam : ICommand
    {
        public string Execute(IDataBase dataBase, IWriter Writer)
        {
            string newTeamName = Writer.WriteLine("Team's name: ");

            dataBase.Teams.AddTeam(newTeamName);

            string activity = $" - Team {newTeamName} was created at { DateTime.Now.ToString("HH:mm:ss")}" +
                              $" on { DateTime.Now.ToString("dd/MM/yyyy")}!";

            var newTeam = dataBase.Teams.GetTeam(newTeamName);
            newTeam.AddToTeamHistory(activity);

            return activity;
        }
    }
}

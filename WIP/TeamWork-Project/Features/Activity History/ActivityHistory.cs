﻿
using System.Collections.Generic;
using TeamWork_Project.Contracts;

namespace WorkItemManager.Features.Activity_History
{
    public class ActivityHistory : IActivityHistory
    {
        private List<string> actHistory;

        public ActivityHistory()
        {
            this.actHistory = new List<string>();
        }

        public IList<string> ActHistory
        {
            get
            {
                return this.actHistory = new List<string>(actHistory);
            }
            set
            {
            }
        }

        public void ShowActivityHistory()
        {
            foreach (string actHistory in ActHistory)
            {
                //actHistory.PrintDetails();
            }
        }
    }
}
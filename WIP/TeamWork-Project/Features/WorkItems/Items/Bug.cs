﻿
using System;
using System.Collections.Generic;
using System.Linq;
using TeamWork_Project.Common;
using TeamWork_Project.Contracts;
using WorkItemManager.Entities;

namespace WorkItemManager.WorkIteams
{
    public class Bug : WorkItem, IBug, IAssignable
    {
        private List<string> stepsToReproduce;
        private Priority priority;
        private Severity severity;
        private IPerson assignee;

        public Bug()
            : base()
        {
            this.stepsToReproduce = new List<string>();
            this.severity = Severity.Minor;
            this.priority = Priority.Low;
            this.status = Status.Active;
        }
        public Bug(IPerson assignee)
        {
            this.Assignee = assignee;
            this.stepsToReproduce = new List<string>();
            this.severity = Severity.Minor;
            this.priority = Priority.Low;
            this.status = Status.Active;
        }
        public Bug(string title, string description, List<Comment> comments, List<History> history,
            List<string> stepsToReproduce, string priority, string severity, IPerson assignee) : base(title, description, comments, history)
        {
            this.stepsToReproduce = new List<string>();
            this.Severity = SetSeverity(severity);
            this.Priority = SetPriority(priority);
            this.status = Status.Active;
        }
        public Bug(string title, string description) : base(title, description)
        {
            this.stepsToReproduce = new List<string>();
            this.severity = Severity.Minor;
            this.priority = Priority.Low;
        }

        public List<string> StepsToReproduce
        {
            get
            {
                return this.stepsToReproduce = new List<string>(stepsToReproduce);
            }
            set { }
        }

        public Priority Priority
        {
            get
            {
                return this.priority;
            }
            set
            {
                this.priority = value;
            }
        }

        public Severity Severity
        {
            get
            {
                return this.severity;
            }
            set
            {
                this.severity = value;
            }
        }

        public IPerson Assignee
        {
            get
            {
                return this.assignee;
            }
            private set
            { }
        }

        public Severity SetSeverity(string trySeverity)
        {
            trySeverity = trySeverity.First().ToString().ToUpper() + String.Join("", trySeverity.Skip(1));
            if (!Enum.TryParse(trySeverity, out Severity severity))
            {
                throw new ArgumentException("Cannot parse parameters");
            }
            return severity;
        }

        public Priority SetPriority(string tryPriority)
        {
            tryPriority = tryPriority.First().ToString().ToUpper() + String.Join("", tryPriority.Skip(1));
            if (!Enum.TryParse(tryPriority, out Priority priority))
            {
                throw new ArgumentException("Cannot parse parameters");
            }
            this.Priority = priority;
            return priority;
        }

        public void ChangeStatus()
        {
            if (this.GetType().Name.ToLower() == "bug")
            {
                if (this.status == Status.Fixed)
                {
                    this.status = Status.Active;
                }
                this.status = Status.Fixed;
            }
        }

        public void SetAssignee(IPerson tryAssignee)
        {
            if (this.assignee != null)
            {
                throw new ArgumentException("This Workitem already has an assignee!");
            }
            this.assignee = tryAssignee;
        }

        public void UnassignPerson()
        {
            if (this.assignee == null)
            {
                throw new ArgumentException("This Workitem has no assignee yet!");
            }
            this.assignee = null;
        }
    }
}
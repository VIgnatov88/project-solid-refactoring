﻿using System;
using System.Linq;
using System.Collections.Generic;
using TeamWork_Project.Common;
using TeamWork_Project.Contracts;
using WorkItemManager.Entities;

namespace WorkItemManager.WorkIteams
{
    public class Story : WorkItem, IStory, IAssignable, IPrioritizable
    {
        private Priority priority;
        private Size size;
        private IPerson assignee;

        public Story()
            : base()
        {
            this.priority = Priority.Low;
            this.size = Size.Small;
            this.status = Status.NotDone;
        }
        public Story(IPerson assignee)
        {
            this.priority = Priority.Low;
            this.size = Size.Small;
            this.status = Status.NotDone;
        }

        public Story(string title, string description, List<Comment> comments, List<History> history, 
            string priority, string size, IPerson assignee) : base (title, description, comments, history)
        {
            this.Priority = priority.ToEnumPriority();
            this.Size = size.ToEnumSize();
            this.Assignee = assignee;
            this.status = Status.NotDone;
        }

        public Story(string title, string description) : base(title, description)
        {
            this.Status = Status.NotDone;
            this.Priority = Priority.Low;
            this.Size = Size.Small;
        }

        public Priority Priority
        {
            get
            {
                return this.priority;
            }
            set { }
        }
        public Size Size
        {
            get
            {
                return this.size;
            }
            set
            {
                ChangeSize(value.ToString());
            }
        }
        public IPerson Assignee
        {
            get
            {
                return this.assignee;
            }
            private set { }
        }

        public override void ChangeStatus(string newValue = "nextDefault")
        {
            if (newValue == "nextDefault")
            {
                int current = Convert.ToInt32(this.status);
                current += 1;
                if (current > 2)
                {
                    Console.WriteLine("This story is already done");
                }
                else
                {
                    this.status = (Status)current;
                }
            }
            else
            {
                newValue.ToLower();

                switch (newValue)
                {
                    case "inprogress": { this.status = Status.Unscheduled; break; }
                    case "done": { this.status = Status.Done; break; }
                }

                this.status = newValue.ToEnumStatus();
            }
        }

        public void ChangeSize(string newValue = "nextDefault")
        {
            if (newValue == "nextDefault")
            {
                int current = Convert.ToInt32(this.status);
                current += 1;
                if (current > 2)
                {
                    Console.WriteLine("This story is already done");
                }
                else
                {
                    this.status = (Status)current;
                }
            }
            else
            {
                newValue.ToLower();

                switch (newValue)
                {
                    case "small": { this.size = Size.Small; break; }
                    case "medium": { this.size = Size.Medium; break; }
                    case "large": { this.size = Size.Large; break; }
                }

                this.status = newValue.ToEnumStatus();
            }
        }

        public void SetAssignee(IPerson tryAssignee)
        {
            if (this.assignee != null)
            {
                throw new ArgumentException("This Workitem already has an assignee!");
            }
            this.assignee = tryAssignee;
        }

        public void UnassignPerson()
        {
            if (this.assignee == null)
            {
                throw new ArgumentException("This Workitem has no assignee yet!");
            }
            this.assignee = null;
        }

        public Priority SetPriority(string tryPriority)
        {
            tryPriority = tryPriority.First().ToString().ToUpper() + String.Join("", tryPriority.Skip(1));
            if (!Enum.TryParse(tryPriority, out Priority priority))
            {
                throw new ArgumentException("Cannot parse parameters");
            }
            this.Priority = priority;
            return priority;
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkItemManager.Entities;

namespace WorkItemManager.WorkIteams
{
    public class History
    {

        private string changes;
        private Person author;
        private DateTime dateTime;

        public History(string changes, Person author)
        {
            this.Changes = changes;
            this.author = author;
            this.dateTime = DateTime.Now;
        }

        public History(string changes)
        {
            this.Changes = changes;
            this.author = null;
        }

        public string Changes
        {
            get
            {
                return this.changes;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException("Comments cannot be null");
                }
                this.changes = value;
            }
        }

        public Person Author
        {
            get
            {
                return this.author;
            }
            set
            {
                this.author = value;
            }
        }

        public DateTime DateTime
        {
            get
            {
                return this.dateTime;
            }
        }


        //public override string ToString()
        //{
        //    return $"{ this.changes}\r\nmade by { this.author} at { this.dateTime.ToString("HH:mm:ss")} on { this.dateTime.ToString("MM/dd/yyyy")}";
        //}

        public string ShowtDetails()
        {
            StringBuilder newSb = new StringBuilder();
            if (this.author == null)
            {
               newSb.Append(changes);
            }
            else
            {
                newSb.AppendLine($" {this.changes}\r\nmade by {this.author} " +
                                 $"at {this.dateTime.ToString("HH:mm:ss")} " +
                                 $"on {this.dateTime.ToString("MM/dd/yyyy")}");
            }
            return newSb.ToString().TrimEnd();
        }

        public virtual List<History> GimmeSomeHistory()
        {
            throw new NotImplementedException();
        }
    }
}

﻿
using System;
using System.Collections.Generic;
using TeamWork_Project.Contracts;
using WorkItemManager.Entities;

namespace WorkItemManager.WorkIteams
{
    public class Comment
    {
        private IPerson author;
        private string message;

        public Comment(string message, IPerson author)
        {
            this.Comments = message;
            this.Author = author;
        }

        public IPerson Author
        {
            get { return this.author; }

            set
            {
                if(value is null)
                {
                    throw new ArgumentException("The input was in invalid format");
                }
                this.author = value;
            }
        }


        public string Comments
        {
            get
            {
                return this.message;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Comments cannot be null or empty");
                }
                this.message = value;
            }
        }

        public void AddCommentToWorkItem()
        {
            if (true)
            {

            }
        }

        public virtual List<Comment> GimmeSomeComment()
        {
            throw new NotImplementedException();
        }
    }
}

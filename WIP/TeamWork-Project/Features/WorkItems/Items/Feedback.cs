﻿using System;
using System.Collections.Generic;
using TeamWork_Project.Common;
using TeamWork_Project.Contracts;

namespace WorkItemManager.WorkIteams
{
    public class Feedback : WorkItem, IFeedback
    {

        private int rating;

        public Feedback()
            : base()
        {
            this.Status = Status.New;
            this.rating = 1;
        }

        public Feedback(string title, string description, List<Comment> comments, List<History> history, int rating)
            : base(title, description, comments, history)
        {
            this.Status = Status.New; 
            this.rating = 1;
        }
        public Feedback(string title, string description, int rating)
            : base(title, description)
        {
            this.Status = Status.New;
            this.rating = CheckRating(rating);
        }
        public int Rating
        {
            get { return this.rating; }
            set { this.rating = CheckRating(value); }
        }
        public override Status Status
        {
            get
            {
                return base.status;
            }
            set
            {
                ChangeStatus(value.ToString());
            }
        }
        public int CheckRating(int inputRating)
        {
            if (inputRating < 1 || inputRating > 5)
            {
                throw new ArgumentException("Your rating must be between 1 and 5 inclusive");
            }
            
            return this.rating = inputRating;
        }

        public override void ChangeStatus(string newValue = "nextDefault")
        {
            if (newValue == "nextDefault")
            {
                int current = Convert.ToInt32(this.status);
                current += 1;
                if (current > 3)
                {
                    Console.WriteLine("This feedback is already done");
                }
                else
                {
                    this.status = (Status)current;
                }
            }
            else
            {
                newValue.ToLower();

                switch (newValue)
                {
                    case "unscheduled": { this.status = Status.Unscheduled; break; }
                    case "scheduled": { this.status = Status.Scheduled; break; }
                    case "done": { this.status = Status.Done; break; }
                }

                this.status = newValue.ToEnumStatus();
            }
        }
    }
}
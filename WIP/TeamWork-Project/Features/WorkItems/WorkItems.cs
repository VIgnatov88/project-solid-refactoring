﻿using System;
using System.Collections.Generic;
using TeamWork_Project.Common;
using TeamWork_Project.Contracts;
using WorkItemManager.Entities;

namespace WorkItemManager.WorkIteams
{
    public abstract class WorkItem : IWorkItem

    {
        //private List<IWorkItem> items;
        private readonly int id;
        private string title;
        private string description;
        protected Status status;
        private List<Comment> comments;
        private List<History> history;
        private List<IPerson> possibleAuthors;
        private const string NULL_OR_EMPTY_EX = "{0} cannot be null or empty!";
        private const string TITLE_OUT_OF_RANGE_EX = "Title must be between 10 and 50 characters!";
        private const string DESCRIPTION_OUT_OF_RANGE_EX = "Description must be between 10 and 500 characters!";

        public WorkItem()
        {
            this.id = IDProvider.GetID();
            this.comments = new List<Comment>();
            this.history = new List<History>();
            this.possibleAuthors = new List<IPerson>(); 
        }

        public WorkItem(string title, string description, IList<Comment> comments, IList<History> history)
        {
            this.id = IDProvider.GetID();
            this.Title = title;
            this.Description = description;
            this.comments = new List<Comment>();
            this.history = new List<History>();
            this.possibleAuthors = new List<IPerson>();
        }

        public WorkItem(string title, string description)
        {
            this.id = IDProvider.GetID();
            this.Title = title;
            this.Description = description;
            this.comments = new List<Comment>();
            this.history = new List<History>();
            this.possibleAuthors = new List<IPerson>();
        }
        public int ID
        {
            get
            {
                return this.id;
            }
        }

        public virtual string Title
        {
            get
            {
                return this.title;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException(string.Format(NULL_OR_EMPTY_EX, "title"));
                }
                if (value.Length < 10 || value.Length > 50)
                {
                    throw new ArgumentException(TITLE_OUT_OF_RANGE_EX);
                }

                this.title = value;
            }
        }

        public string Description
        {
            get
            {
                return this.description;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException(string.Format(NULL_OR_EMPTY_EX, "description"));
                }
                if (value.Length < 10 || value.Length > 500)
                {
                    throw new ArgumentException(DESCRIPTION_OUT_OF_RANGE_EX);
                }

                this.description = value;
            }
        }


        public List<Comment> Comments
        {
            get
            {
                return new List<Comment>(this.comments);
            }
        }

        public List<History> History
        {
            get
            {
                return new List<History>(this.history);
            }
        }

        public List<IPerson> PossibleAuthors
        {
            get
            {
                return this.possibleAuthors = new List<IPerson>(possibleAuthors);
            }
            private set
            {

            }
        }

        public void SetPossibleAuthors(List<IPerson> authors)
        {
            this.possibleAuthors = authors;
        }

        public virtual Status Status
        {
            get
            {
                return this.status;
            }
            set { }
        }

        public void AddComment(string message, IPerson author)
        {
            this.comments = new List<Comment>(this.comments);

            if (!PossibleAuthorsContain(author))
            {
                throw new ArgumentException("Only team members can leave comments!"); 
            }

            Comment newComment = new Comment(message, author);
            this.comments.Add(newComment);
        }

        public bool PossibleAuthorsContain(IPerson tryAuthor)
        {

            foreach (var possibleAuthor in this.possibleAuthors)
            {
                if (possibleAuthor.Name == tryAuthor.Name)
                {
                    return true;
                }
            }
            return false;
        }

        public void AddToHistory(string message)
        {
            //this.history = new List<History>(this.history);
            //if (!this.possibleAuthors.Contains(author))
            //{
            //    throw new ArgumentException("Only team members can comment!");
            //}
            History newHistory = new History(message);
            this.history.Add(newHistory);
        }

        public virtual void ChangeStatus(string newValue)
        {
            newValue.ToEnumStatus();
        }

        public virtual bool IsExisting(string newTitle)
        {
            if (this.title == newTitle)
            {
                return true;
            }
            return false;
        }

        public override string ToString()
        {
            string commentCount = this.Comments.Count == 1 ? "comment" : "comments";
            string historyCount = this.History.Count == 1 ? "history" : "histories";

            return $"{this.GetType().Name} with ID {this.ID} and title \"{this.title}\"\nDescription: {this.description}\n" +
                $"Status: {this.Status}\n{this.Comments.Count} {commentCount}\n{this.History} {historyCount}";

        }
    }
}
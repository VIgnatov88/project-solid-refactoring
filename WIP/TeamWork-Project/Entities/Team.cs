﻿
using System;
using System.Collections.Generic;
using System.Text;
using TeamWork_Project.Contracts;
using WorkItemManager.WorkIteams;

namespace WorkItemManager.Entities
{
    public class Team
    {
        private List<IBoard> boards;
        private List<History> activityHistory;
        private Members members;
        private string name;

        public Team(string name)
        {
            this.Name = name;
            this.Members = new Members();
            this.boards = new List<IBoard>();
            this.activityHistory = new List<History>();
        }

        public List<IBoard> Boards
        {
            get
            {
                return this.boards = new List<IBoard>(boards);
            }

        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Name cannot be null or empty");
                }
                else if (value.Length < 5 || value.Length > 15)
                {
                    throw new ArgumentException("Name must be between 5 and 15 characters inclusive");
                }
                this.name = value;
            }
        }

        public Members Members { get; set; }


        public List<History> ActivityHistory
        {
            get
            {
                return this.activityHistory = new List<History>(activityHistory);
            }
        }


        /// <summary>
        /// Add object of Type Person to Team members.
        /// </summary>
        /// <param name="Person"></param>
        public void AddMember(IPerson person)
        {
            this.Members.AddPerson(person);
        }

        /// <summary>
        /// Set new Board for object of type Team.
        /// </summary>
        /// <param name="board"></param>
        public void SetBoard(IBoard board)
        {
            if (BoardExistsInTeam(board.Name))
                throw new ArgumentException("Such board already exists!");

            this.boards.Add(board);
        }

        /// <summary>
        /// Get all instances of type Person for the current Team.
        /// Returns all Members as String.
        /// </summary>
        /// <returns>Return String of Members</returns>
        public string GetAllTeamMembers()
        {
            return new StringBuilder(this.Members.ShowAllPeople()).ToString();
        }

        public bool BoardExistsInTeam(string name)
        {
            foreach (var board in boards)
            {
                if (board.Name == name)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Show all activities of object Team
        /// </summary>
        /// <returns>Return Activity history as String</returns>
        public string ShowTeamActivity()
        {
            StringBuilder activityBuilder = new StringBuilder();
            foreach (History history in activityHistory)
                activityBuilder.AppendLine(history.ShowtDetails());

            string trimmed = activityBuilder.ToString();
            return trimmed.Remove(trimmed.LastIndexOf(Environment.NewLine));
        }

        /// <summary>
        /// Get all active Boards for the current Team.
        /// </summary>
        /// <returns>Return object of type Board</returns>
        public string GetAllBoards()
        {
            StringBuilder teamBoardsBuilder = new StringBuilder();
            foreach (var board in this.boards)
                teamBoardsBuilder.AppendLine(" - " + board.Name);

            string trimmed = teamBoardsBuilder.ToString();
            return trimmed.Remove(trimmed.LastIndexOf(Environment.NewLine));
        }

        /// <summary>
        /// Check if Team contains object of Type WorkItem by given Key
        /// </summary>
        /// <param name="Title"></param>
        /// <returns>Return object of type WorkItem</returns>
        public IWorkItem TeamContainsWorkitem(string tryTitle, string tryType)
        {
            foreach (var board in boards)
                return board.FindWorkItemByTitle(tryTitle, tryType);

            throw new ArgumentException("Workitem with such title does not exist");
        }

        /// <summary>
        /// Add history to object of type Team
        /// </summary>
        /// <param name="Message"></param>
        public void AddToTeamHistory(string message)
        {
            History newHistory = new History(message);
            this.activityHistory.Add(newHistory);
        }

        public void AddToBoardHistory(IBoard seekedBoard, string message)
        {
            foreach (var board in this.boards)
            {
                if (board == seekedBoard)
                {
                    board.AddToHistory(message);
                }
            }
        }

        public string ListWorkitemsInTeamByTitle()
        {
            
            var builder = new StringBuilder();
            foreach (var board in boards)
            {
                builder.AppendLine(board.ListWorkitemsByTitle());
            }
            string trimmed = builder.ToString();
            return trimmed.Remove(trimmed.LastIndexOf(Environment.NewLine));
        }

        public string ListAllWorkitemsInTeam()
        {
            var builder = new StringBuilder();
            foreach (var board in boards)
            {
                builder.AppendLine(board.ListAllWorkitemsInBoard());
            }
            string trimmed = builder.ToString();
            return trimmed.Remove(trimmed.LastIndexOf(Environment.NewLine));
        }

        public string ListWorkitemsInTeamFilteredByBug()
        {
            var builder = new StringBuilder();
            foreach (var board in boards)
            {
                builder.AppendLine(board.ListWorkitemsFilteredByBug());
            }
            string trimmed = builder.ToString();
            return trimmed.Remove(trimmed.LastIndexOf(Environment.NewLine));
        }

        public string ListWorkitemsInTeamFilteredByStory()
        {
            var builder = new StringBuilder();
            foreach (var board in boards)
            {
                builder.AppendLine(board.ListWorkitemsFilteredByStory());
            }
            string trimmed = builder.ToString();
            return trimmed.Remove(trimmed.LastIndexOf(Environment.NewLine));
        }

        public string ListWorkitemsInTeamFilteredByFeedback()
        {
            var builder = new StringBuilder();
            foreach (var board in boards)
            {
                builder.AppendLine(board.ListWorkitemsFilteredByFeedback());
            }
            string trimmed = builder.ToString();
            return trimmed.Remove(trimmed.LastIndexOf(Environment.NewLine));
        }

        public string ListWorkitemsInTeamFilteredByStatus(string tryValue)
        {
            var builder = new StringBuilder();
            foreach (var board in boards)
            {
                builder.AppendLine(board.ListWorkitemsFilteredByStatus(tryValue));
            }
            string trimmed = builder.ToString();
            return trimmed.Remove(trimmed.LastIndexOf(Environment.NewLine));
        }

        public IBoard GetBoardFromTeam(string name)
        {
            foreach (var board in this.boards)
            {
                if (board.Name == name)
                {
                    return board;
                }
            }
            throw new ArgumentException("Such board does not exist!");
        }

        public IWorkItem GetWorkItemFromTeam(string title, string tryType)
        {
            IWorkItem newItem = null;

            foreach (var board in this.boards)
            {
                newItem = board.GetWorkItemFromBoard(title, tryType);
                if (newItem != null)
                {
                    return newItem;
                }
            }

            return newItem;
        }
    }
}


﻿
using System;
using System.Collections.Generic;
using System.Text;
using TeamWork_Project.Contracts;
using WorkItemManager.WorkIteams;

namespace WorkItemManager.Entities
{
    public class Person : IPerson
    {
        private string name;
        private List<History> activityHistory;
        private List<IWorkItem> workItems;

        public Person(string name)
        {
            this.Name = name;
            this.activityHistory = new List<History>();
            this.workItems = new List<IWorkItem>();
        }

        public List<History> ActivityHistory
        {
            get { return this.activityHistory; }
            private set { }
        }
        
        public List<IWorkItem> WorkItems
        {
            get
            {
                return this.workItems = new List<IWorkItem>(this.workItems);
            }
            set { }
           }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Name cannot be null or empty");
                }
                else if (value.Length < 5 || value.Length > 15)
                {
                    throw new ArgumentException("Name must be between 5 and 15 characters inclusive");
                }
                this.name = value;
            }
        }

        /// <summary>
        /// Remove work item that is assigned to object of Type Person
        /// </summary>
        /// <param name="workItemName"></param>
        public void RemoveWorkItem(string itemName)
        {
            foreach (IWorkItem workItem in this.WorkItems)
            {
                if (workItem.Title.Equals(itemName))
                    this.WorkItems.Remove(workItem);
            }
        }

        /// <summary>
        /// List All activity history of specific Person
        /// Returns the history as String of activities
        /// </summary>
        /// <returns>Returns List of History objects</returns>
        public string ListHistory()
        {
            StringBuilder builder = new StringBuilder();

            foreach (History history in activityHistory)
            {
                if (!(history.Author == null))
                {
                    builder.AppendLine(history.ToString());
                }
                else
                {
                    builder.AppendLine(history.Changes);
                }
                
            }
            string trimmed = builder.ToString().Remove(builder.ToString().LastIndexOf(Environment.NewLine));
            return trimmed;
        }

        /// <summary>
        /// Chech if object of Type WorkItem exist
        /// </summary>
        /// <param name="Title"></param>
        /// <returns>return Boolean: true/false</returns>
        public bool CheckIfWorkItemExists(string tryTitle)
        {
            foreach (var item in workItems)
            {
                if (item.Title == tryTitle)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Get object of Type WorkItem by given Key
        /// Returns an instance of WorkItem as IWorkItem
        /// </summary>
        /// <param name="tryTitle"></param>
        /// <returns>return IWorkItem</returns>
        public IWorkItem GetWorkItem(string tryTitle)
        {
            foreach (var item in workItems)
            {
                if (item.Title == tryTitle)
                    return item;
            }
            throw new ArgumentException("No such Item!");
        }
    }
}



﻿
using System;
using System.Collections.Generic;
using System.Text;
using TeamWork_Project.Contracts;

namespace WorkItemManager.Entities
{
    public class Members
    {
        private Dictionary<string, IPerson> AllMembers;

        public Members()
        {
            AllMembers = new Dictionary<string, IPerson>();
        }

    
        /// <summary>
        /// Add an object of type Person to Members
        /// </summary>
        /// <param name="Person"></param>
        /// 
        public void AddPerson(IPerson person)
        {
            AllMembers.Add(person.Name, person);
        }
        
        /// <summary>
        /// Returns an object by given Key.
        /// </summary>
        /// <param name="key"></param>
        public IPerson GetPerson(string name)
        {
            if (!AllMembers.ContainsKey(name))
                throw new ArgumentException("No such person!");
            return AllMembers[name];
        }

        /// <summary>
        /// Returns list of All People as string.
        /// </summary>
        public string ShowAllPeople()
        {
            if (AllMembers.Count == 0)
                throw new ArgumentException("There are no people!");

            StringBuilder personBuilder = new StringBuilder();
            foreach (string key in AllMembers.Keys)
                personBuilder.AppendLine(" - " + AllMembers[key].Name); 
            

            string trimmed = personBuilder.ToString();
            return trimmed.Remove(trimmed.LastIndexOf(Environment.NewLine));
        }

        /// <summary>
        /// Check if a Collection contains object of type WorkItem.
        /// </summary>
        /// <param name="tryValue"></param>
        /// <returns>
        /// True/False
        /// </returns>
        public bool MemberPossessesWorkItem(string tryValue)
        {
            foreach (Person person in AllMembers.Values)
            {
                if (person.CheckIfWorkItemExists(tryValue))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Check if a Collection contains object of given Key.
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public bool ContainsMember(string tryName)
        {
            foreach (var item in AllMembers.Keys)
            {
                if (tryName == item)
                    return true;
            }
            return false;
        }
        
        /// <summary>
        /// Returns an object of type WorkItem.
        /// </summary>
        /// <param name="Title"></param>
        public IWorkItem GetWorkItem(string tryTitle)
        {
            foreach (Person person in AllMembers.Values)
               return person.GetWorkItem(tryTitle);
            
            throw new ArgumentException("No such team!");
        }

        /// <summary>
        /// Returns all members as object - List<>
        /// </summary>
        /// <returns></returns>
        public List<IPerson> MembersAsList()
        {
            List<IPerson> members = new List<IPerson>();
            foreach (var member in this.AllMembers.Values)
            {
                members.Add(member);
            }
            return members;
        }
    }
}

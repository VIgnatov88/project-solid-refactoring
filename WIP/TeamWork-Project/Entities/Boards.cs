﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamWork_Project.Common;
using TeamWork_Project.Common.Extensions;
using TeamWork_Project.Contracts;
using WorkItemManager.WorkIteams;

namespace WorkItemManager.Entities
{
    public class Boards : IBoard
    {
        private string name;
        private List<IWorkItem> workItems;
        private List<Person> possibleAuthors;
        private List<History> activityHistory;

        public Boards()
        {
            this.name = null;
            this.workItems = new List<IWorkItem>();
            this.possibleAuthors = new List<Person>();
            this.activityHistory = new List<History>();
        }

        public Boards(string name)
        {
            this.Name = name;
            this.workItems = new List<IWorkItem>();
            this.possibleAuthors = new List<Person>();
            this.activityHistory = new List<History>();
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Board name cannot be null or empty");
                }
                else if (value.Length < 5 || value.Length > 10)
                {
                    throw new ArgumentException("Board name must be between 5 and 10 characters inclusive");
                }
                this.name = value;
            }
        }

        public List<IPerson> PossibleAuthors
        {
            get
            {
                return new List<IPerson>(this.possibleAuthors);
            }
            set
            { }
        }

        public List<IWorkItem> WorkItems
        {
            get
            {
                return this.workItems = new List<IWorkItem>(this.workItems);
            }
            set { }
        }
        public List<History> ActivityHistory
        {
            get
            {
                return this.activityHistory = new List<History>(activityHistory);
            }
            set { }
        }

        /// <summary>
        /// Show all committed operations in the specific board.
        /// </summary>
        /// <returns>Returns a String with all operations.</returns>
        public string ShowBoardActivity()
        {
            StringBuilder historyBuilder = new StringBuilder();
            foreach (History history in activityHistory)
                historyBuilder.AppendLine(history.ShowtDetails());

            return historyBuilder.ToString();
        }

        /// <summary>
        /// Check the current instance if workItem exist.
        /// </summary>
        /// <param name="workItemName"></param>
        /// <param name="Key"></param>
        /// <returns>Returns Boolean: true/false</returns>
        public bool WorkItemExistsInBoard(string workItemName, string tryType)
        {
            string type = string.Empty;

            EnumType result;
            bool isParseSuccess = EnumType.TryParse(tryType, true, out result);
            if (isParseSuccess)
                type = tryType;

            if (workItems.Count == 0)
                return false;

            foreach (IWorkItem item in workItems)
                if (item.Title == workItemName && item.GetType().Name.ToString() == type)
                    return true;

            return false;
        }

        /// <summary>
        /// Check if object Board contains object WorkItem
        /// </summary>
        /// <param name="Title"></param>
        /// <param name="Type"></param>
        /// <returns>Return IWorkItem</returns>
        public IWorkItem FindWorkItemByTitle(string tryTitle, string tryType)
        {
            foreach (var item in workItems)
            {
                if (item.Title == tryTitle && item.GetType().Name.ToLower() == tryType)
                {
                    return item;
                }
            }
            throw new ArgumentException("Workitem with such title does not exist");
        }

        /// <summary>
        /// Add String message to object Board history
        /// </summary>
        /// <param name="Message"></param>
        public void AddToHistory(string message)
        {
            History newHistory = new History(message);
            this.activityHistory.Add(newHistory);
        }

        public string ListWorkitemsByTitle()
        {
            var sorted = this.workItems.OrderBy(x => x.Title);
            var builder = new StringBuilder();
            foreach (var item in sorted)
            {
                builder.AppendLine($" - {item.Title} (type {item.GetType().Name})");
            }
            string trimmed = builder.ToString();
            return trimmed.Remove(trimmed.LastIndexOf(Environment.NewLine));
        }

        public string ListAllWorkitemsInBoard()
        {
            var sorted = this.workItems;
            var builder = new StringBuilder();
            foreach (var item in sorted)
            {
                builder.AppendLine($" - {item.Title} (type {item.GetType().Name})");
            }
            string trimmed = builder.ToString();
            return trimmed.Remove(trimmed.LastIndexOf(Environment.NewLine));
        }

        public string ListWorkitemsFilteredByBug()
        {
            var filteredList = this.workItems.Where(w => w.GetType().Name.ToLower() == "bug");
            var builder = new StringBuilder();

            foreach (var item in filteredList)
            {
                builder.AppendLine($" - {item.Title}");
            }
            string trimmed = builder.ToString();
            if (trimmed.Length < 1)
            {
                return " There are no existing workitems of type bug";
            }
            return $" All workitems filtered by Bug:\n{ trimmed.Remove(trimmed.LastIndexOf(Environment.NewLine))}";
        }

        public string ListWorkitemsFilteredByStory()
        {
            var filteredList = this.workItems.Where(w => w.GetType().Name.ToLower() == "story");
            var builder = new StringBuilder();

            foreach (var item in filteredList)
            {
                builder.AppendLine($" - {item.Title}");
            }
            string trimmed = builder.ToString();

            if (trimmed.Length < 1)
            {
                return " There are no existing workitems of type story";
            }
            return $" All workitems filtered by Story:\n{ trimmed.Remove(trimmed.LastIndexOf(Environment.NewLine))}";
        }

        public string ListWorkitemsFilteredByFeedback()
        {
            var filteredList = this.workItems.Where(w => w.GetType().Name.ToLower() == "feedback");
            var builder = new StringBuilder();

            foreach (var item in filteredList)
            {
                builder.AppendLine($" - {item.Title}");
            }
            string trimmed = builder.ToString();

            if (trimmed.Length < 1)
            {
                return " There are no existing workitems of type feedback";
            }
            return $" All workitems filtered by Feedback:\n{ trimmed.Remove(trimmed.LastIndexOf(Environment.NewLine))}";
        }

        public string ListWorkitemsFilteredByStatus(string tryValue)
        {
            var filteredList = this.workItems.Where(w => w.Status.Equals(EnumHelper.ToEnumStatus(tryValue)));
            var builder = new StringBuilder();

            foreach (var item in filteredList)
            {
                builder.AppendLine($" - {item.Title}");
            }
            string trimmed = builder.ToString();

            if (trimmed.Length < 1)
            {
                return " There are no workitems with this status!";
            }
            return $" All workitems filtered by Status:\n{trimmed.Remove(trimmed.LastIndexOf(Environment.NewLine))}";
        }

        public void AddWorkitemToBoard(IWorkItem newItem)
        {
            this.workItems.Add(newItem);
        }

        public IWorkItem GetWorkItemFromBoard(string title, string tryType)
        {
            IWorkItem newItem = null;
            foreach (var item in this.workItems)
            {
                if (item.Title == title && item.GetType().Name.ToLower() == tryType)
                {
                    newItem = item;
                }
            }
            return newItem;
        }
    }
}


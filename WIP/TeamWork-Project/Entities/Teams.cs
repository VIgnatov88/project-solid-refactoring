﻿
using System;
using System.Collections.Generic;
using System.Text;
using TeamWork_Project.Contracts;
using WorkItemManager.Entities;

namespace WorkItemManager.Core
{
    public class Teams
    {
        private Dictionary<string, Team> teams;

        public Teams()
        {
            this.teams = new Dictionary<string, Team>();
        }

        /// <summary>
        /// Add new object of type Team to Instance of Teams.
        /// </summary>
        /// <param name="teamName"></param>
        public void AddTeam(string name)
        {
            if (teams.ContainsKey(name))
                throw new ArgumentException($"Team with name {name} already exist!");
            teams.Add(name, new Team(name));
        }

        /// <summary>
        /// Show all objects of type Team by their Name.
        /// </summary>
        /// <returns>Returns List of names as String</returns>
        public string ShowAllTeams()
        {
            StringBuilder teamBuilder = new StringBuilder();
            if (teams.Keys.Count == 0)
                throw new ArgumentException("There aren't any teams created");

            foreach (string teamsKey in teams.Keys)
                teamBuilder.AppendLine(" - " + teamsKey);

            string trimmed = teamBuilder.ToString();
            return trimmed.Remove(trimmed.LastIndexOf(Environment.NewLine));
        }

        /// <summary>
        /// Show all objects of type Person in given object type Team.
        /// </summary>
        /// <param name="teamName"></param>
        /// <returns>Returns List of names as String</returns>
        public string ShowAllTeamMembers(string teamName)
        {
            if (!teams.ContainsKey(teamName))
                throw new ArgumentException($"NO Team with name {teamName}!");

            StringBuilder teamBuilder = new StringBuilder(teams[teamName].GetAllTeamMembers());

            return teamBuilder.ToString();
        }

        /// <summary>
        /// Show all activity history of object type Team.
        /// </summary>
        /// <param name="Team"></param>
        /// <returns>Return activities as String</returns>
        public string ShowAllTeamActivity(Team team)
        {
            return team.ShowTeamActivity().ToString();
        }

        /// <summary>
        /// Get object of type Team by given name.
        /// </summary>
        /// <param name="Name"></param>
        /// <returns>Return object of type Team</returns>
        public Team GetTeam(string name)
        {
            if (!teams.ContainsKey(name))
                throw new ArgumentException("No such team!");

            return teams[name];
        }

        public IBoard GetBoard(string name)
        {
            IBoard board = new Boards();
            foreach (var team in teams.Values)
            {
                board = team.GetBoardFromTeam(name);
            }
            return board;
        }

        /// <summary>
        /// Check if object of type WorkItems exist in Team by given Title
        /// </summary>
        /// <param name="Title"></param>
        /// <returns>Return object of type WorkItem</returns>
        public IWorkItem GetWorkItemByTitleAndType(string tryTitle, string tryType)
        {
            foreach (var team in teams.Values)
                return team.TeamContainsWorkitem(tryTitle, tryType);

            throw new ArgumentException("Workitem with such title does not exist");
        }

        public string ShowBoardActivity(string boardName)
        {
            foreach (var team in teams.Values)
            {
                var boards = team.Boards;
                foreach (var board in boards)
                {
                    if (board.Name == boardName)
                    {
                        return board.ShowBoardActivity();
                    }
                }
            }
            throw new ArgumentException("Board with such name does not exist!");
        }


        /// <summary>
        /// Check if object of type Person exist in Team by given Name
        /// </summary>
        /// <param name="Name"></param>
        /// <returns>Return Boolean: true/false<returns>
        public bool CheckIfPersonIsTeamMember(string tryName)
        {
            var teamList = teams.Values;

            foreach (var team in teamList)
            {
                if (team.Members.ContainsMember(tryName))
                    return true;
            }
            return false;
        }

        public string ListAllExistingWorkitemsInTeamByTitle()
        {
            var builder = new StringBuilder();
            foreach (var team in teams.Values)
            {
                builder.AppendLine(team.ListWorkitemsInTeamByTitle());
            }
            string trimmed = builder.ToString();
            return trimmed.Remove(trimmed.LastIndexOf(Environment.NewLine));
        }

        public string ListAllExistingWorkitems()
        {
            var builder = new StringBuilder();
            foreach (var team in teams.Values)
            {
                builder.AppendLine(team.ListAllWorkitemsInTeam());
            }
            string trimmed = builder.ToString();
            return trimmed.Remove(trimmed.LastIndexOf(Environment.NewLine));
        }

        public string ListAllExistingWorkitemsInTeamFilteredByBug()
        {
            var builder = new StringBuilder();
            foreach (var team in teams.Values)
            {
                builder.AppendLine(team.ListWorkitemsInTeamFilteredByBug());
            }
            string trimmed = builder.ToString();
            return trimmed.Remove(trimmed.LastIndexOf(Environment.NewLine));
        }

        public string ListAllExistingWorkitemsInTeamFilteredByStory()
        {
            var builder = new StringBuilder();
            foreach (var team in teams.Values)
            {
                builder.AppendLine(team.ListWorkitemsInTeamFilteredByStory());
            }
            string trimmed = builder.ToString();
            return trimmed.Remove(trimmed.LastIndexOf(Environment.NewLine));
        }

        public string ListAllExistingWorkitemsInTeamFilteredByFeedback()
        {
            var builder = new StringBuilder();
            foreach (var team in teams.Values)
            {
                builder.AppendLine(team.ListWorkitemsInTeamFilteredByFeedback());
            }
            string trimmed = builder.ToString();
           
            return trimmed.Remove(trimmed.LastIndexOf(Environment.NewLine));
        }

        public string ListAllExistingWorkitemsInTeamFilteredByStatus(string tryValue)
        {
            var builder = new StringBuilder();
            foreach (var team in teams.Values)
            {
                builder.AppendLine(team.ListWorkitemsInTeamFilteredByStatus(tryValue));
            }
            string trimmed = builder.ToString();
            return trimmed.Remove(trimmed.LastIndexOf(Environment.NewLine));
        }

        public IWorkItem GetWorkItem(string title, string tryType)
        {
            IWorkItem newItem = null;

            foreach (var team in this.teams.Values)
            {
                newItem = team.GetWorkItemFromTeam(title, tryType);
                if (newItem != null)
                {
                    return newItem;
                }
            }
            return newItem;
        }

        public List<IPerson> GetPossibleAuthors(string boardName)
        {
            var authors = new List<IPerson>();

            foreach (var team in this.teams.Values)
            {
                var boards = team.Boards;
                foreach (var board in boards)
                {
                    if (board.Name == boardName)
                    {
                        return authors = team.Members.MembersAsList();
                        
                    }
                }
            }
            return authors;
        }
    }
}


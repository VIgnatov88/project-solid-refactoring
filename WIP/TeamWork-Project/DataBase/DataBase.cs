﻿
namespace WorkItemManager.DataBase
{
    using WorkItemManager.Core;
    using WorkItemManager.Entities;

    public class DataBase : IDataBase
    {
        public DataBase()
        {
            this.Members = new Members();
            this.Teams = new Teams();
            this.Boards = new Boards();
        }

        public Members Members { get; }

        public Teams Teams { get; }

        public Boards Boards { get; }
    }
}
﻿
namespace WorkItemManager.DataBase
{
    using WorkItemManager.Core;
    using WorkItemManager.Entities;

    public interface IDataBase
    {
        Members Members { get; }

        Teams Teams { get; }

        Boards Boards { get; }
    }
}
﻿
using WorkItemManager.DataBase;

namespace WorkItemManager
{
    using System.Linq;
    using System.Reflection;
    using Autofac;
    using TeamWork_Project.Contracts;
    using WorkItemManager.Core;
    using WorkItemManager.Core.Contracts;
    using WorkItemManager.Entities;

    public class AppBuilder : IBuilderInstance
    {
        public void BuildApp()
        {

            // Initializes a new instance of the ContainerBuilder class
            // From Control container - AutoFac
            var appBuilder = new ContainerBuilder();
            appBuilder
                .RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .AsImplementedInterfaces();


            // Register a component to be created through reflection
            // Resolve a single instance component
            // Always get the same instance no matter where it's required
            appBuilder.RegisterType<DataBase.DataBase>().As<IDataBase>().SingleInstance();
            appBuilder.RegisterType<Members>().As<Members>().SingleInstance();
            appBuilder.RegisterType<Teams>().As<Teams>().SingleInstance();
            appBuilder.RegisterType<Boards>().As<Boards>().SingleInstance();


            // Register a component to be created through reflection
            // Provide a String that can be used to retrieve the component
            // Register all the commands that will be needed 
            // With Interface ICommand used as a Type for the command
            var commands = Assembly.GetExecutingAssembly()
                .DefinedTypes.Where(
                    typeInfo => typeInfo.ImplementedInterfaces.Contains(typeof(ICommand)))
                .ToList();
            foreach (var command in commands)
            {
                appBuilder.RegisterType(command.AsType()).Named<ICommand>(command.Name.ToLower());
            }

            
            // Build all the registrations that have been made
            // And make a new Container
            // Start the application
            var container = appBuilder.Build();
            var engine = container.Resolve<IRun>();
            engine.Run();
        }
    }
}
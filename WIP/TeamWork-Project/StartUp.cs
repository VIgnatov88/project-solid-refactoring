﻿
using System;

namespace WorkItemManager
{
    class StartUp
    {
        public static void Main()
        {
            Console.Title = "WIM";
            new AppBuilder().BuildApp();
        }
    }
}


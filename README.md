
Team-management platform C-Sharp Project � BSKS




** Please navigate upwards or downwards with the arrows to choose operation

** When command is selected you will be automatically asked and suggested what text to input based on the chosen command.

** By default at starting point the application will have nothing existing

** Your last command and its result will be displayed on the screen

--------------------------------------
DEMO
--------------------------------------


** Create new person ** (You will be asked to give specific name to the person)

  � Creates a new individual with given name in the program.
  � If person with this exact name exists you will be informed and you should re-select the command and enter new name.

** Add person to team (You will be asked to specify the names of the person and the team)

- If the specified names do not correspond to existing person/team you will be notified that there�s no such person/team

